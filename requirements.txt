Babel>=1.3
Flask>=0.10.1
Flask-Babel>=0.9
Flask-Cors>=1.9.0
Flask-Login>=0.2.9
Flask-Moment>=0.4.0
Flask-SQLAlchemy>=1.0
Flask-Script>=2.0.5
Flask-Migrate>=1.3.0
Flask-Session>=0.2
Jinja2>=2.7.2
Mako>=0.9.1
MarkupSafe>=0.18
SQLAlchemy>=0.9.3
Werkzeug>=0.9.4
alembic>=0.6.3
beautifulsoup4>=4.0
requests>=2.9