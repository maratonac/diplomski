#!flask/Scripts/python
import os
from app import create_app, db
from flask.ext.script import Server, Manager
from flask.ext.migrate import Migrate, MigrateCommand

from app import model

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)
server = Server(host="0.0.0.0", port=80)

manager.add_command('db', MigrateCommand)
manager.add_command('runserver', server)

if __name__ == '__main__':
    manager.run()
