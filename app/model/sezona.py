from app import db
from app.model.kolo import Kolo


class Sezona(db.Model):

    __tablename__ = "tbl_sezona"

    id = db.Column(db.Integer, primary_key=True)
    id_lige = db.Column(db.Integer, db.ForeignKey('tbl_liga.id'))

    sezona = db.Column(db.String(7))
    broj_kola = db.Column(db.Integer)
    trenutno_kolo = db.Column(db.Integer)
    kola = db.relationship("Kolo", lazy="joined")
    sampion = db.Column(db.Integer, db.ForeignKey('tbl_klub.id'))

    def __init__(self, sezona, broj_kola):
        self.sezona = sezona
        self.broj_kola = broj_kola
        self.trenutno_kolo = 0
        self.kola = []

    def dodaj_kolo(self):
        kolo = Kolo(self.trenutno_kolo)
        self.trenutno_kolo += 1
        self.kola.append(kolo)
        return kolo

    @classmethod
    def dohvati_sve_sezone(cls, backoffice=False):
        result = cls.query.order_by(cls.sezona.asc()).all()
        final_result = list()
        if backoffice:
            for sezona in result:
                temp = dict()
                temp["sezona"] = sezona.sezona
                temp["broj_kola"] = sezona.broj_kola
                final_result.append(temp)
        else:
            for sezona in result:
                final_result.append(sezona.sezona)

        return final_result

    @classmethod
    def dohvati_broj_kola(cls, sezona=False):
        if sezona:
            result = cls.query.filter(cls.sezona == sezona).with_entities(cls.broj_kola).first()
            return result[0]
        return False

    @classmethod
    def dohvati_sezonu(cls, sezona):
        return cls.query.filter(cls.sezona == sezona).first()

    @classmethod
    def dohvati_poslednju_sezonu(cls):
        return cls.query.order_by(cls.id.desc()).first()

    def izmeni_kolo(self, kolo, rezultati):
        for rezultat in self.kola[kolo].rezultati:
            for rez in rezultati:
                if rez["id"] == rezultat.id:
                    rezultat.golovi_domacin_ukupno = rez["golovi_domacin_ukupno"]
                    rezultat.golovi_gost_ukupno = rez["golovi_gost_ukupno"]
                    rezultat.golovi_domacin_poluvreme = rez["golovi_domacin_poluvreme"]
                    rezultat.golovi_gost_poluvreme = rez["golovi_gost_poluvreme"]
                    if rezultat.golovi_domacin_ukupno > rezultat.golovi_gost_ukupno:
                        rezultat.rezultat = "1"
                    elif rezultat.golovi_domacin_ukupno == rezultat.golovi_gost_ukupno:
                        rezultat.rezultat = "X"
                    else:
                        rezultat.rezultat = "2"
                    rezultat.rezultat_golovi_konacno = "{}:{}".format(
                        rezultat.golovi_domacin_ukupno, rezultat.golovi_gost_ukupno)
                    db.session.add(rezultat)
                    break
        db.session.commit()

        return "ok"

