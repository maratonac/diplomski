from app.model.liga import Liga
from app.model.klub import Klub
from app.model.kolo import Kolo
from app.model.rezultat import Rezultat
from app.model.sezona import Sezona
from app.model.user import User