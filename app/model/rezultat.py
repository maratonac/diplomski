from app import db


class Rezultat(db.Model):

    __tablename__ = "tbl_rezultat"

    id = db.Column(db.Integer, primary_key=True)
    id_domacin = db.Column(db.Integer, db.ForeignKey("tbl_klub.id"))
    id_gost = db.Column(db.Integer, db.ForeignKey('tbl_klub.id'))
    domacin = db.Column(db.String(128))
    gost = db.Column(db.String(128))
    golovi_domacin_ukupno = db.Column(db.Integer)
    golovi_domacin_poluvreme = db.Column(db.Integer)
    golovi_gost_ukupno = db.Column(db.Integer)
    golovi_gost_poluvreme = db.Column(db.Integer)
    rezultat = db.Column(db.String(1))
    id_kolo = db.Column(db.Integer, db.ForeignKey('tbl_kolo.id'))
    sezona = db.Column(db.String(7))
    rezultat_golovi_konacno = db.Column(db.String(10))
    rezultat_golovi_poluvreme = db.Column(db.String(10))

    def __init__(self, id_domacin, id_gost, domacin, gost, golovi_domacin_ukupno,
                 golovi_domacin_poluvreme, golovi_gost_ukupno, golovi_gost_poluvreme, rezultat,
                 sezona, rezultat_golovi_konacno, rezultat_golovi_poluvreme):
        self.id_domacin = id_domacin
        self.id_gost = id_gost
        self.domacin = domacin
        self.gost = gost
        self.golovi_domacin_ukupno = golovi_domacin_ukupno
        self.golovi_domacin_poluvreme = golovi_domacin_poluvreme
        self.golovi_gost_ukupno = golovi_gost_ukupno
        self.golovi_gost_poluvreme = golovi_gost_poluvreme
        self.rezultat = rezultat
        self.sezona = sezona
        self.rezultat_golovi_konacno = rezultat_golovi_konacno
        self.rezultat_golovi_poluvreme = rezultat_golovi_poluvreme

    @classmethod
    def dohvati_sve(cls):
        return cls.query.all()

    @classmethod
    def dohvati_podatke_za_drugi_grafik(cls):
        final_result = list()
        sql = "select count(r.rezultat), r.rezultat " \
                "from tbl_rezultat r " \
                "group by r.rezultat;"

        result = db.engine.execute(sql)
        for row in result:
            temp = dict()
            temp["broj"] = row[0]
            temp["rezultat"] = row[1]
            final_result.append(temp)
        return final_result

    @classmethod
    def dohvati_podatke_za_treci_grafik(cls):
        final_result = dict()
        sql = "SELECT ROUND((SUM(r.golovi_domacin_ukupno)) / COUNT(r.id),2) as domaci," \
              "ROUND((SUM(r.golovi_gost_ukupno)) / COUNT(r.id),2) as gosti " \
              "FROM tbl_rezultat r;"

        result = db.engine.execute(sql)
        final_result["ukupno"] = list()
        for row in result:
            temp = dict()
            temp["domaci"] = row[0]
            temp["gosti"] = row[1]
            final_result["ukupno"].append(temp)
        return final_result

    @classmethod
    def dohvati_odnos_pobede_porazi(cls, klub_id, sezone, protivnici=False):
        final_result = list()

        sql = "(select count(*) as rezultati " \
              "from tbl_rezultat r " \
              "where ((r.id_domacin = {0} and r.golovi_domacin_ukupno > r.golovi_gost_ukupno) " \
              "or (r.id_gost = {0} and r.golovi_gost_ukupno > r.golovi_domacin_ukupno)) " \
              "and sezona in ({1})) " \
              "union all " \
              "(select count(*) " \
              "from tbl_rezultat r " \
              "where ((r.id_domacin = {0} and r.golovi_domacin_ukupno = r.golovi_gost_ukupno) " \
              "or (r.id_gost = {0} and r.golovi_gost_ukupno = r.golovi_domacin_ukupno)) " \
              "and sezona in ({1})) " \
              "union all " \
              "(select count(*) " \
              "from tbl_rezultat r " \
              "where ((r.id_domacin = {0} and r.golovi_domacin_ukupno < r.golovi_gost_ukupno) " \
              "or (r.id_gost = {0} and r.golovi_gost_ukupno < r.golovi_domacin_ukupno)) " \
              "and sezona in ({1}));".format(klub_id, str(sezone).strip("[]"))
        if protivnici:
            sql = "(select count(*) as rezultati " \
              "from tbl_rezultat r " \
              "where ((r.id_domacin = {0} and r.golovi_domacin_ukupno > r.golovi_gost_ukupno and r.id_gost in ({2})) " \
              "or (r.id_gost = {0} and r.golovi_gost_ukupno > r.golovi_domacin_ukupno and r.id_domacin in ({2}))) " \
              "and sezona in ({1})) " \
              "union all " \
              "(select count(*) " \
              "from tbl_rezultat r " \
              "where ((r.id_domacin = {0} and r.golovi_domacin_ukupno = r.golovi_gost_ukupno and r.id_gost in ({2})) " \
              "or (r.id_gost = {0} and r.golovi_gost_ukupno = r.golovi_domacin_ukupno and r.id_domacin in ({2}))) " \
              "and sezona in ({1})) " \
              "union all " \
              "(select count(*) " \
              "from tbl_rezultat r " \
              "where ((r.id_domacin = {0} and r.golovi_domacin_ukupno < r.golovi_gost_ukupno and r.id_gost in ({2})) " \
              "or (r.id_gost = {0} and r.golovi_gost_ukupno < r.golovi_domacin_ukupno and r.id_domacin in ({2}))) " \
              "and sezona in ({1}));".format(klub_id, str(sezone).strip("[]"), str(protivnici).strip("[]"))

        result = db.engine.execute(sql)
        result_list = list()
        for row in result:
            result_list.append(row)

        print(result_list)
        temp_result = dict()
        temp_result["ime"] = "pobede"
        temp_result["vrednost"] = result_list[0][0]
        final_result.append(temp_result)
        temp_result = dict()
        temp_result["ime"] = "nereseno"
        temp_result["vrednost"] = result_list[1][0]
        final_result.append(temp_result)
        temp_result = dict()
        temp_result["ime"] = "porazi"
        temp_result["vrednost"] = result_list[2][0]
        final_result.append(temp_result)
        return final_result

    @classmethod
    def dohvati_formu(cls, klub_id, sezone, limit=10, protivnici=False):
        final_result = list()
        sql = "select r.id, r.domacin, r.gost, r.golovi_domacin_ukupno, r.golovi_gost_ukupno, k.broj_kola, sezona " \
              "(case " \
              "when r.id_domacin = {0} and r.golovi_domacin_ukupno > r.golovi_gost_ukupno then \"1\"" \
              "when r.id_gost = {0} and r.golovi_gost_ukupno > r.golovi_domacin_ukupno then \"1\"" \
              "when r.golovi_domacin_ukupno = r.golovi_gost_ukupno then \"X\"" \
              "when r.id_domacin = {0} and r.golovi_domacin_ukupno < r.golovi_gost_ukupno then \"2\"" \
              "when r.id_gost = {0} and r.golovi_gost_ukupno < r.golovi_domacin_ukupno then \"2\"" \
              "end) as rezultat, " \
              "if(r.id_domacin = {0}, kl_g.grb, kl_d.grb) as grb " \
              "from tbl_rezultat r " \
              "inner join tbl_kolo k on r.id_kolo = k.id " \
              "inner join tbl_klub kl_d on r.id_domacin = kl_d.id " \
              "inner join tbl_klub kl_g on r.id_gost = kl_g.id " \
              "where" \
              "(r.id_domacin = {0} or r.id_gost = {0}) " \
              "and " \
              "sezona in ({1}) " \
              "order by sezona desc, broj_kola desc " \
              "limit {2};".format(klub_id, str(sezone).strip("[]"), limit)
        if protivnici:
            sql = "select r.id, r.domacin, r.gost, r.golovi_domacin_ukupno, r.golovi_gost_ukupno, " \
                  "k.broj_kola, sezona," \
                  "(case " \
                  "when r.id_domacin = {0} and r.id_gost in ({3}) and r.golovi_domacin_ukupno > r.golovi_gost_ukupno then \"1\"" \
                  "when r.id_gost = {0} and r.id_domacin in ({3}) and r.golovi_gost_ukupno > r.golovi_domacin_ukupno then \"1\"" \
                  "when r.golovi_domacin_ukupno = r.golovi_gost_ukupno then \"X\"" \
                  "when r.id_domacin = {0} and r.id_gost in ({3}) and r.golovi_domacin_ukupno < r.golovi_gost_ukupno then \"2\"" \
                  "when r.id_gost = {0} and r.id_domacin in ({3}) and r.golovi_gost_ukupno < r.golovi_domacin_ukupno then \"2\"" \
                  "end) as rezultat, " \
                  "if(r.id_domacin = {0}, kl_g.grb, kl_d.grb) as grb " \
                  "from tbl_rezultat r " \
                  "inner join tbl_kolo k on r.id_kolo = k.id " \
                  "inner join tbl_klub kl_d on r.id_domacin = kl_d.id " \
                  "inner join tbl_klub kl_g on r.id_gost = kl_g.id " \
                  "where" \
                  "((r.id_domacin = {0} and r.id_gost in({3})) or (r.id_gost = {0} and r.id_domacin in ({3}))) " \
                  "and " \
                  "sezona in ({1}) " \
                  "order by sezona desc, broj_kola desc " \
                  "limit {2};".format(klub_id, str(sezone).strip("[]"), limit, str(protivnici).strip("[]"))

        result = db.engine.execute(sql)

        from app.utils.asset_url import asset_url

        for row in result:
            rezultat = dict()
            rezultat["id"] = row[0]
            rezultat["domacin"] = row[1]
            rezultat["gost"] = row[2]
            rezultat["golovi_domacin"] = row[3]
            rezultat["golovi_gost"] = row[4]
            rezultat["kolo"] = row[5]
            rezultat["sezona"] = row[6]
            rezultat["rezultat"] = row[7]
            rezultat["grb"] = asset_url('slike/{}'.format(row[8]))
            final_result.append(rezultat)

        return final_result

    @classmethod
    def dohvati_rezultate(cls, klub_id, sezone=False, protivnici=False):

        final_result = list()
        sql = "select " \
              "count(*), golovi_domacin, golovi_gost " \
              "from " \
              "(select golovi_domacin_ukupno as golovi_domacin, golovi_gost_ukupno as golovi_gost " \
              "from tbl_rezultat " \
              "where id_domacin = {0} " \
              "{1} " \
              "union all " \
              "select golovi_gost_ukupno, golovi_domacin_ukupno " \
              "from tbl_rezultat " \
              "where id_gost = {0} " \
              "{2} " \
              ") as a " \
              "group by golovi_domacin, golovi_gost;"

        uslov_domacin = ""
        uslov_gost = ""
        if sezone != False:
            uslov_domacin = "and sezona in ({0}) ".format(str(sezone).strip("[]"))
            uslov_gost = "and sezona in ({0}) ".format(str(sezone).strip("[]"))
        if protivnici != False:
            uslov_domacin += "and id_gost in ({0}) ".format(str(protivnici).strip("[]"))
            uslov_gost += "and id_domacin in ({0}) ".format(str(protivnici).strip("[]"))

        sql = sql.format(klub_id, uslov_domacin, uslov_gost)

        result = db.engine.execute(sql)
        for row in result:
            rezultat = dict()
            rezultat["broj"] = row[0]
            rezultat["golovi_domacin"] = row[1]
            rezultat["golovi_gost"] = row[2]
            rezultat["rezultat"] = "{}:{}".format(row[1], row[2])
            final_result.append(rezultat)

        return final_result

    @classmethod
    def dohvati_rezultate_za_sezonu_kolo(cls, sezona=False, kolo=False):
        if sezona and kolo > -1:
            sql = "select " \
                  "r.domacin, kl1.grb," \
                  "r.gost, kl2.grb," \
                  "r.golovi_domacin_ukupno, r.golovi_gost_ukupno," \
                  "r.golovi_domacin_poluvreme, r.golovi_gost_poluvreme, r.id " \
                  "from tbl_rezultat r " \
                  "left join tbl_kolo k on r.id_kolo = k.id " \
                  "left join tbl_klub kl1 on r.id_domacin = kl1.id " \
                  "left join tbl_klub kl2 on r.id_gost = kl2.id " \
                  "where r.sezona = \"{}\" and k.broj_kola = {};".format(sezona, kolo)

            result = db.engine.execute(sql)
            final_result = list()
            from app.utils.asset_url import asset_url
            for row in result:
                rezultat = dict()
                rezultat["domacin"] = row[0]
                rezultat["grb_domacin"] = asset_url('slike/{}'.format(row[1]))
                rezultat["gost"] = row[2]
                rezultat["grb_gost"] = asset_url('slike/{}'.format(row[3]))
                rezultat["golovi_domacin_ukupno"] = row[4]
                rezultat["golovi_gost_ukupno"] = row[5]
                rezultat["golovi_domacin_poluvreme"] = row[6]
                rezultat["golovi_gost_poluvreme"] = row[7]
                rezultat["id"] = row[8]
                final_result.append(rezultat)
            return final_result
        return False

