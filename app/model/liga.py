from app import db



class Liga(db.Model):

    __tablename__ = "tbl_liga"

    id = db.Column(db.Integer, primary_key=True)
    ime = db.Column(db.String(128))
    skracenica = db.Column(db.String(16))
    klubovi = db.relationship("Klub", lazy="joined")
    sezone = db.relationship("Sezona")

    def __init__(self, name="", short_name=""):
        self.ime = name
        self.skracenica = short_name

    @classmethod
    def nadji_ligu_po_skracenici(cls, skracenica):
        return cls.query.filter(cls.skracenica == skracenica).first()

    @classmethod
    def nadji_ligu_po_idu(cls, liga_id):
        return cls.query.filter(cls.id == liga_id).first()
