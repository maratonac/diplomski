from app import db
from app.utils.custom_types import JSONEncodedDict


class Kolo(db.Model):

    __tablename__ = "tbl_kolo"

    id = db.Column(db.Integer, primary_key=True)
    id_sezone = db.Column(db.Integer, db.ForeignKey('tbl_sezona.id'))
    broj_kola = db.Column(db.Integer)
    rezultati = db.relationship("Rezultat", lazy="joined")

    tabela = db.Column(JSONEncodedDict)

    def __init__(self, broj_kola):
        self.broj_kola = broj_kola
        self.rezultati = []

    def dodaj_rezultat(self, rezultat):
        self.rezultati.append(rezultat)
