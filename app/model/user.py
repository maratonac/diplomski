from app import db
from app import login_manager
from datetime import date


@login_manager.user_loader
def load_user(user_id):
    print("User id: {}".format(user_id))
    user = User.query.get(int(user_id))
    if user:
        return user


class User(db.Model):

    __tablename__ = "tbl_user"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128))
    username = db.Column(db.String(128), unique=True)
    password = db.Column(db.String(128))
    ime = db.Column(db.String(128))
    prezime = db.Column(db.String(128))
    platio = db.Column(db.Boolean)
    datum_isteka = db.Column(db.Date, default=False)
    backoffice = db.Column(db.Boolean)

    def __init__(self, email, username, password, ime, prezime):
        self.email = email
        self.username = username
        self.password = password
        self.ime = ime
        self.prezime = prezime
        self.platio = False
        self.datum_isteka = False
        self.backoffice = False

    @classmethod
    def registruj(cls, email, username, password, confirmPassword, ime, prezime):

        registruj = True
        errors = dict()
        import re

        if email == "":
            registruj = False
            errors["email"] = "Morate popuniti polje!"
        elif not re.match(r"[^@]+@[^@]+\.[^@]+", email):
            registruj = False
            errors["email"] = "Email nije validan"
        elif cls.query.filter(cls.email == email).all():
            registruj = False
            errors["email"] = "Email vec postoji u bazi"
        if username == "":
            registruj = False
            errors["username"] = "Morate popuniti polje"
        elif cls.query.filter(cls.username == username).all():
            registruj = False
            errors["username"] = "Korisnicko ime je zauzeto!"
        if password == "":
            registruj = False
            errors["password"] = "Morate popuniti polje!"
        if confirmPassword == "":
            registruj = False
            errors["confirm-password"] = "Morate popuniti polje!"

        if password != confirmPassword:
           registruj = False
           errors["password"] = "Sifre nisu iste"

        if not registruj:
            return "error", errors

        user = User(email, username, password, ime, prezime)

        db.session.add(user)
        db.session.commit()

        return "ok", False


    @classmethod
    def login(cls, username, password, backoffice=False):

        login = True
        errors = dict()

        if username == "":
            login = False
            errors["username"] = "Morate popuniti polje"
        if password == "":
            login = False
            errors["password"] = "Morate popuniti polje!"
        if not login:
            return "error", errors

        if not backoffice:
            user = cls.query.filter(cls.username == username, cls.password == password).first()
        else:
            user = cls.query.filter(cls.username == username, cls.password == password, cls.backoffice == True).first()
        if not user:
            errors["username"] = "Ne postoji korisnik"
            return "error", errors

        from flask.ext.login import login_user

        login_user(user)

        return "ok", False

    @classmethod
    def get_all(cls):
        return cls.query.filter(cls.backoffice == 0).all()

    @classmethod
    def get_by_id(cls, id):
        return cls.query.filter(cls.id == id).first()

    def to_json(self):
        user_dict = dict()
        user_dict["email"] = self.email
        user_dict["username"] = self.username
        user_dict["ime"] = self.ime
        user_dict["prezime"] = self.prezime
        user_dict["platio"] = self.platio
        if self.datum_isteka:
            user_dict["datum_isteka"] = "{}-{}-{}".format(
                self.datum_isteka.year, self.datum_isteka.month, self.datum_isteka.day)

        import json
        return json.dumps(user_dict)

    def is_active(self):
        return True

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return "{}".format(self.id)
        except AttributeError:
            raise NotImplementedError('No `id` attribute - override `get_id`')


    def izmeni(self, email, username, ime, prezime):
        self.ime = ime
        self.prezime = prezime
        self.email = email
        self.username = username

        db.session.add(self)
        db.session.commit()

        result = dict()
        result["status"] = "ok"
        return result

    def izmeni_password(self, password, cpassword):

        result = dict()

        if password == "":
            result['status'] = "error"
            result["message"] = "Password ne moze da bude prazan"
        elif cpassword == "":
            result['status'] = "error"
            result["message"] = "CPassword ne moze da bude prazan"
        elif password != cpassword:
            result['status'] = "error"
            result["message"] = "Passwordi nisu isti"
        else:
            self.password = password
            db.session.add(self)
            db.session.commit()
            result["status"] = "ok"

        return result
