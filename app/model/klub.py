from app import db

FILE_EXTENSIONS = {"image/jpeg": "jpg",
                   "image/jpg": "jpg",
                   "image/png": "png",
                   "image/svg+xml": "svg"}


class Klub(db.Model):

    __tablename__ = "tbl_klub"

    id = db.Column(db.Integer, primary_key=True)
    ime = db.Column(db.String(128))
    skracenica = db.Column(db.String(16))
    grb = db.Column(db.String(256))
    id_lige = db.Column(db.Integer, db.ForeignKey("tbl_liga.id"))
    opis_kluba = db.Column(db.Text, default="")

    def __init__(self, ime="", skracenica="", grb=""):
        self.ime = ime
        self.skracenica = skracenica
        self.grb = grb

    def dodaj_klub(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def dohvati_klub_id(cls, klub_id):
        return cls.query.filter(cls.id == klub_id).first()

    @classmethod
    def dohvati_klub_ime(cls, ime):
        return cls.query.filter(cls.ime == ime).first()

    @classmethod
    def dohvati_sve_klubove(cls):
        return cls.query.all()

    @classmethod
    def dohvati_sve_klubove_dict(cls):

        result = cls.query.all()
        final_result = list()
        for klub in result:
            klub_dict = dict()
            klub_dict["ime"] = klub.ime
            klub_dict["id"] = klub.id
            final_result.append(klub_dict)
        return final_result

    @classmethod
    def dohvati_klub_skracenica(cls, skracenica):
        return cls.query.filter(cls.skracenica == skracenica).first()

    @classmethod
    def dodaj_klub(cls, podaci):

        import base64
        from app.utils.asset_url import asset_url
        from app.model.liga import Liga
        result = dict()

        ime = podaci["ime"]
        skracenica = podaci["skracenica"]
        opis = podaci["opis"]
        file_name = ""
        if "grb" in podaci and "type" in podaci:
            grb = podaci["grb"]
            type = podaci["type"]
            file_bytes = bytes(grb, 'utf-8')
            file_stream = base64.b64decode(file_bytes)
            file_length = len(file_bytes)
            if int(file_length) != int(len(grb)):
                result["status"] = "error"
                result["message"] = "Error uploading file"
                return result
            extension = ""
            if type in FILE_EXTENSIONS:
                extension = FILE_EXTENSIONS[type]
            file_name = skracenica + "." + extension

            print(os.path.join('/root/diplomski/diplomski/app', "static", "slike", file_name))
            import os
            file_object = open(os.path.join('/root/diplomski/diplomski/app', "static", "slike", file_name), 'w+b')
            # file_object = open("/static/slike/" + file_name, "w+b")
            file_object.write(file_stream)
            file_object.close()

        klub = Klub(ime, skracenica, file_name)
        klub.opis_kluba = opis
        db.session.add(klub)
        liga = Liga.nadji_ligu_po_skracenici("sl")
        liga.klubovi.append(klub)
        db.session.commit()

        result["status"] = "ok"
        result["klub"] = dict()
        result["klub"]["id"] = klub.id
        result["klub"]["ime"] = klub.ime
        result["klub"]["grb"] = asset_url("slike/{}".format(klub.grb))

        return result

    def izmeni(self, podaci):

        ime = podaci["ime"]
        skracenica = podaci["skracenica"]
        opis = podaci["opis"]

        import base64
        result = dict()

        if "grb" in podaci and "type" in podaci:
            grb = podaci["grb"]
            type = podaci["type"]
            file_bytes = bytes(grb, 'utf-8')
            file_stream = base64.b64decode(file_bytes)
            file_length = len(file_bytes)
            if int(file_length) != int(len(grb)):
                result["status"] = "error"
                result["message"] = "Error uploading file"
                return result
            extension = ""
            if type in FILE_EXTENSIONS:
                extension = FILE_EXTENSIONS[type]
            file_name = skracenica + "." + extension

            import os
            # obrisi stari fajl
            grb_putanja = os.path.join('/root/diplomski/diplomski/app', "static", "slike", self.grb)
            if os.path.isfile(grb_putanja):
                os.remove(grb_putanja)
            file_object = open(os.path.join('/root/diplomski/diplomski/app', "static", "slike", file_name), 'w+b')
            file_object.write(file_stream)
            file_object.close()
            self.grb = file_name

        self.ime = ime
        self.skracenica = skracenica

        self.opis_kluba = opis

        db.session.add(self)
        db.session.commit()

        result["status"] = "ok"

        return result
