from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.session import Session
from config import config
from app.utils.asset_url import asset_url
from app.utils.datetime import datetime
from flask.ext.login import LoginManager
from flask import redirect, request

db = SQLAlchemy()
sess = Session()
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'main.index'

@login_manager.unauthorized_handler
def unauthorized():
    # do stuff
    print(request.url)
    if "backoffice" in request.url:
        return redirect("/backoffice")

    return redirect("/klubovi#register")

def create_app(config_name=None):

    if not config_name:
        import os
        config_name = os.getenv('FLASK_CONFIG') or 'default'

    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)
    login_manager.init_app(app)
    sess.init_app(app)
    sess.permanent = True

    app.jinja_env.globals.update(asset_url=asset_url)
    app.jinja_env.filters["datetime"] = datetime
    from .routing.main import main as main_blueprint
    from .routing.data import data as data_blueprint
    from .routing.user import user as user_blueprint
    from .routing.backoffice import backoffice as backoffice_blueprint
    app.register_blueprint(main_blueprint)
    app.register_blueprint(data_blueprint, url_prefix="/data")
    app.register_blueprint(user_blueprint, url_prefix="/user")
    app.register_blueprint(backoffice_blueprint, url_prefix="/backoffice")

    return app
