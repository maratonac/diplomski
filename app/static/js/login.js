$(document).ready(function(){
    var loginContainer = $(".login .container");
    var login = $('.login');
    var loginForm = $('.login form');
    $("#show-login").click(function(e){
        e.preventDefault();
        loginForm.find(".error-label").text("");
        loginForm.find("input").removeClass("input-error");
        login.fadeIn(300);
        var marginTop = ($(window).height() - loginContainer.height()) / 2;
        loginContainer.css("margin-top", marginTop + "px");
    });

    $(".login-close").click(function(e){
        e.preventDefault();
        login.fadeOut(300);
    });

    $("#login-start").click(function(e){
        e.preventDefault();
        var data = {
            username : $("#login-username").val(),
            password : $("#login-password").val()
        }

        $.ajax({
            type: "POST",
            url: "/user/login",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            dataType: "json",
            success: successLogin,
            error: errorLogin
        });

        function successLogin(data){
            loginForm.find(".error-label").text("");
            loginForm.find("input").removeClass("input-error");
            if (data.status == "ok") {
                window.location.reload();
            }else{
                for (var i in data.errors){
                    $("#login-" + i).addClass("input-error");
                    $("#login-error-" + i).text(data.errors[i]);
                }
            }
        }
        function errorLogin(data){
            console.log("error");
            console.log(data);
        }
    })
});