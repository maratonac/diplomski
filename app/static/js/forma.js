(function FormaCtrl(window){

    FormaCtrl.prototype.mainElement = false;
    FormaCtrl.prototype.data = false;
    FormaCtrl.prototype.sirina = false;
    FormaCtrl.prototype.visina = false;

    var svg;
    var bar;
    var x;
    var y;
    var xAxis;
    var yAxis;

    var imageSize = 50;

    var tip;

    var margin = {top: 20, right: 30, bottom: 30, left: 40};

    function FormaCtrl(){}

    FormaCtrl.prototype.init = function(mainElement,data, sirina, visina) {
        this.mainElement = mainElement;
        this.data = data;
        this.sirina = sirina - margin.left - margin.right;
        this.visina = visina - margin.top - margin.bottom;

        x = d3.scale.ordinal()
            .rangeRoundPoints([0,this.sirina],.5);
        y = d3.scale.ordinal()
            .domain(["1","X","2"])
            .rangeRoundPoints([0,this.visina],.5);

        xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");
        yAxis = d3.svg.axis()
            .scale(y)
            .orient("left");

        svg = d3.select(this.mainElement)
            .attr("width", "100%")
            .attr("height", "100%")
            .append("g")
            .attr("transform", "translate("+margin.left + "," + margin.right + ")");

        tip = d3.select(".tooltip");

        x.domain(data.map(function(d,i){ return data.length - i;}));

        var barWidth = x.rangeBand();
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + this.visina + ")")
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis);

        bar = svg
            .selectAll('.bar')
            .data(data)
            .enter()
            .append('image')
            .attr("class", "bar")
            .attr("xlink:href", function(d){ return d.grb; })
            .attr("x", function(d,i){ return x(i+1) - imageSize / 2;})
            .attr('y', function(d){return y(d.rezultat) -imageSize / 2;})
            .attr('height', imageSize)
            .attr('width', imageSize)
            .on("mouseover", function(d){
                tip.transition()
                    .duration(200)
                    .style("opacity",.9);
                tip.html(
                    "<strong>Sezona: "+d.sezona+", Kolo: "+ d.kolo +"<br>"+
                    d.domacin + " : " + d.gost + "<br>Rezultat: " + d.golovi_domacin + ":" + d.golovi_gost + "</strong>"
                )
                    .style("left", (d3.event.pageX) + "px")
                    .style("top", (d3.event.pageY - 28) + "px");
            })
            .on("mouseout", function(d){
                tip.transition()
                    .duration(200)
                    .style("opacity", 0);
            });
    };

    FormaCtrl.prototype.update = function(new_data) {

        this.data = new_data;

        x.domain(new_data.map(function(d,i){ return new_data.length - i;}));

        svg.selectAll('.bar')
            .data(this.data)
            .enter()
            .append('image')
            .attr("class", "bar")
            .on("mouseover", function(d){
                tip.transition()
                    .duration(200)
                    .style("opacity",.9);
                tip.html(
                    "<strong>Sezona: "+d.sezona+", Kolo: "+ d.kolo +"<br>"+
                    d.domacin + " : " + d.gost + "<br>Rezultat: " + d.golovi_domacin + ":" + d.golovi_gost + "</strong>"
                )
                    .style("left", (d3.event.pageX) + "px")
                    .style("top", (d3.event.pageY - 28) + "px");
            })
            .on("mouseout", function(d){
                tip.transition()
                    .duration(200)
                    .style("opacity", 0);
            });

        svg.selectAll('.bar')
            .data(this.data)
            .exit()
            .remove();

        svg.selectAll('.bar').transition().duration(750)
            .attr("x", function(d,i){ return x(i+1) - imageSize / 2;})
            .attr('y', function(d){return y(d.rezultat) - imageSize / 2;})
            .attr("xlink:href", function(d){ console.log(d.grb); return d.grb; })
            .attr('height', imageSize)
            .attr('width', imageSize);
        svg.select(".x.axis")
            .transition()
            .duration(750)
            .call(xAxis);
    };

    window.FormaCtrl = FormaCtrl;

}(window));