var Filter = (function(){

    var cls = function() {
        var sezone = false;
        var klubovi = false;
        var html = false;
        var rezultati = false;
        var filter = false;
        var primeniDugme = false;

        this.setSezone = function(nove_sezone){
            sezone = nove_sezone;
        };
        this.getSezone = function(){
            return sezone;
        };
        this.setKlubovi = function(novi_klubovi){
            klubovi = novi_klubovi;
        };
        this.getKlubovi = function(){
            return klubovi;
        };
        this.toggleFilterValue = function(){
            filter = !filter;
        };
        this.getFilter = function(){
            return filter;
        };
        this.setPrimeniDugme = function(novo_dugme){
            primeniDugme = novo_dugme;
        };
        this.getPrimeniDugme = function(){
            return primeniDugme;
        };
        this.setHtml = function(novi_html){
            html = novi_html;

            d3.selectAll(html + ' [name=klubovi]').on('change',function(){
                var value = this.value;
                var pozicija = klubovi.indexOf(value);
                if (this.value == "sve"){
                    var that = this;
                    d3.selectAll(html + ' [name=klubovi]')
                            .filter(function(){return this.value!="sve"})
                            .property("checked", that.checked).each(changeKlubovi);
                }else{
                    if (this.checked) {
                        if (pozicija == -1){
                            klubovi.push(value);
                        }
                    }else{
                        if (pozicija != -1){
                            klubovi.splice(pozicija, 1);
                            d3.selectAll(html + ' [name=klubovi]')
                                    .filter(function(){return this.value == "sve"})
                                    .property("checked", false);
                        }
                    }
                }
            });

            d3.selectAll(html + ' [name=sezone]').on('change',function(){
                var value = this.value;
                var pozicija = sezone.indexOf(value);
                if (this.value == "sve"){
                    var that = this;
                    d3.selectAll(html + ' [name=sezone]')
                            .filter(function(){return this.value!="sve"})
                            .property("checked", that.checked).each(changeSezone);
                }else{
                    if (this.checked) {
                        if (pozicija == -1){
                            console.log("append");
                            sezone.push(value);
                        }
                    }else{
                        if (pozicija != -1){
                            console.log("remove");
                            sezone.splice(pozicija, 1);
                            d3.selectAll(html + ' [name=sezone]')
                                    .filter(function(){return this.value == "sve"})
                                    .property("checked", false);
                        }
                    }
                }
                console.log(sezone);
            });

        };
        this.getHtml = function(){
            return html;
        };
        this.setRezultati = function(novi_rezultati){
            rezultati = novi_rezultati;
        };
        this.getRezultati = function(){
            return rezultati;
        };

        function changeSezone(){
            var value = this.value;
            var pozicija = sezone.indexOf(value);
            if (this.checked) {
                if (pozicija == -1){
                    sezone.push(value);
                }
            }else{
                if (pozicija != -1){
                    sezone.splice(pozicija, 1);
                }
            }
        }

        function changeKlubovi(){
            var value = this.value;
            var pozicija = klubovi.indexOf(value);
            if (this.checked) {
                if (pozicija == -1){
                    klubovi.push(value);
                }
            }else{
                if (pozicija != -1){
                    klubovi.splice(pozicija, 1);
                }
            }
        }
    };

    cls.prototype = {
        prikazi: function() {
            console.log("***");
            console.log("Id: " + this.getHtml());
            console.log("Klubovi: " + this.getKlubovi());
            console.log("Sezone: " + this.getSezone());
            console.log("Rezultati: " + this.getRezultati());
        },
        setuj: function(html_id, sezone, klubovi, rezultati, primeniDugme){
            this.setHtml(html_id);
            this.setSezone(sezone);
            this.setKlubovi(klubovi);
            this.setRezultati(rezultati);
            this.setPrimeniDugme(primeniDugme);
        },
        toggleFilter: function(){
            this.toggleFilterValue();
            d3.select(this.getHtml()).transition().duration(350).style('display', this.getFilter() ? "block" : "none");
            d3.select(this.getPrimeniDugme()).style('display', this.getFilter() ? "block" : "none");
        }
    };

    return cls;
})();
