$(document).ready(function(){

    var registerContainer = $(".register .container");
    var register = $(".register");
    var registerForm = $(".register form");
    var registerSuccess = $(".register-success");
    $("#show-register").click(function(e){
        e.preventDefault();
        registerForm.find(".error-label").text("");
        registerForm.find("input").removeClass("input-error");
        registerForm.show();
        registerSuccess.hide();
        register.fadeIn(300);
        var marginTop = ($(window).height() - registerContainer.height()) / 2;
        registerContainer.css("margin-top", marginTop + "px");
    });

    $(".register-close").click(function(e){
        e.preventDefault();
        register.fadeOut(300);
    });

    $("#register-start").click(function(e){
        e.preventDefault();
        var data = {
            email : $("#register-email").val(),
            username: $("#register-username").val(),
            password : $("#register-password").val(),
            confirmPassword : $("#register-confirm-password").val(),
            name : $("#register-name").val(),
            lastName : $("#register-last-name").val()
        };

        $.ajax({
            type: "POST",
            url: "/user/register",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            dataType: "json",
            success: successRegister,
            error: errorRegister
        })

    });

    function successRegister(data){
        registerForm.find(".error-label").text("");
        registerForm.find("input").removeClass("input-error");
        if (data.status == "ok") {
            registerForm.hide();
            registerSuccess.fadeIn(100);
        }else{
            for (var i in data.errors){
                $("#register-" + i).addClass("input-error");
                $("#register-error-" + i).text(data.errors[i]);

            }
        }

    }

    function errorRegister(data){
        console.log("error");
        console.log(data);
    }

    if (window.location.hash == "#register"){
        window.location.hash = "";
        $("#show-register").trigger("click");
    }
});