$(document).ready(function(){

    var loginForm = $('form.login-modal');

   $("#login-start").click(function(e){
        e.preventDefault();
        var data = {
            username : $("#login-username").val(),
            password : $("#login-password").val()
        };

        $.ajax({
            type: "POST",
            url: "/user/backoffice/login",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            dataType: "json",
            success: successLogin,
            error: errorLogin
        });

        function successLogin(data){
            loginForm.find(".error-label").text("");
            loginForm.find("input").removeClass("input-error");
            if (data.status == "ok") {
                window.location = "/backoffice/rezultati";
            }else{
                for (var i in data.errors){
                    $("#login-" + i).addClass("input-error");
                    $("#login-error-" + i).text(data.errors[i]);
                }
            }
        }
        function errorLogin(data){
            console.log("error");
            console.log(data);
        }
    })
});