$(document).ready(function(){

    var izmeniModal = $(".izmeni-korisnika-modal")

    $(".izmeni-korisnika").click(function(){

        var params = {
            user_id : $(this).data('id')
        };
        izmeniModal.find("input[type=text], input[type=email], input[type=hidden]").val("");
        $.ajax({
            type: "POST",
            url: "/user/backoffice/dohvati_korisnika",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            success: function(data){
                if (data.status == "ok"){
                    $("#user_id").val(data.user.id);
                    $("#user-email").val(data.user.email);
                    $("#user-username").val(data.user.username);
                    $("#user-name").val(data.user.ime);
                    $("#user-last-name").val(data.user.prezime);

                    $(".izmeni-korisnik").fadeIn(300);
                }else{
                    alert(data.message);
                }
            },
            error: function(error){
                console.log(error);
                alert("error");
            }
        })
    });

    $(".izmeni-close").click(function(e){
        e.preventDefault();
        $(".izmeni-korisnik").fadeOut(300);
    });

    $("#izmeni-start").click(function(e){
        e.preventDefault();

        var params = {
            user_id : $("#user_id").val(),
            email : $("#user-email").val(),
            username : $("#user-username").val(),
            ime : $("#user-name").val(),
            prezime : $("#user-last-name").val()
        };

        $.ajax({
            type: "POST",
            url: "/user/backoffice/izmeni_korisnika",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            success: function(data){
                if (data.status == "ok"){
                    location.reload()
                }else{
                    alert(data.message);
                }
            },
            error: function(error){
                console.log(error);
                alert("error");
            }
        })
    })
});