$(document).ready(function(){

    var sezonaSelect = $("#sezona");
    var koloSelect = $("#kolo");
    var prikaziKolo = $("#prikazi_kolo");
    var fileByteArray = false;
    var fileToUpload = false;
    var klubIzmena = false;

    sezonaSelect.change(function(){
        var data = {
            sezona : sezonaSelect.find('option:selected').val()
        };
        koloSelect.find('option').remove();
        koloSelect.append('<option value="-1">--</option>');
        if (data.sezona != "-1") {
            $.ajax({
                type: "POST",
                url: "/user/backoffice/dohvati_broj_kola",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json",
                success: successSezona,
                error: errorSezona
            });

            function successSezona(data){

                for(var i=1;i<=data.broj_kola;i++){
                    koloSelect.append('<option value="'+(i-1)+'">'+i+'</option>');
                }
            }
            function errorSezona(data){
                console.log("error");
                console.log("data");
            }
        }

    });

    koloSelect.change(function(){
        var data = {
            sezona : sezonaSelect.find('option:selected').val(),
            kolo : Number(koloSelect.find('option:selected').val())
        };
        if (data.sezona != "-1" && data.kolo != -1){
            $.ajax({
                type: "POST",
                url: "/user/backoffice/dohvati_kolo",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json",
                success: successKolo,
                error: errorKolo
            });

            function successKolo(data){
                prikaziKolo.html("");
                if (data.status == "ok"){
                    prikaziKolo.html(data.html);
                }else{
                    alert(data.message);
                }
            }

            function errorKolo(data){
                console.log("error");
                console.log("data");
            }
        }
    });
    $(document).on('click','#rezultat-edit',function(){
        $(".info-mode").hide();
        $(".edit-mode").show();
    });
    $(document).on('click','#rezultat-odustani',function(){
        $(".edit-mode").hide();
        $(".info-mode").show();
    });
    $(document).on('click','#rezultat-izmeni',function(){
        var rezultati = [];
        prikaziKolo.find(".rezultat").each(function(){
            var that = $(this);
            var rezultat = {
                id : Number(that.data("id")),
                golovi_domacin_ukupno : Number(that.find('.golovi_domacin_ukupno').val()),
                golovi_gost_ukupno : Number(that.find('.golovi_gost_ukupno').val()),
                golovi_domacin_poluvreme : Number(that.find('.golovi_domacin_poluvreme').val()),
                golovi_gost_poluvreme : Number(that.find('.golovi_gost_poluvreme').val())
            };

            rezultati.push(rezultat);
        });
        var data = {
            sezona : sezonaSelect.find('option:selected').val(),
            kolo : Number(koloSelect.find('option:selected').val()),
            rezultati : rezultati
        };
        $.ajax({
            type: "POST",
            url: "/user/backoffice/izmeni_kolo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            dataType: "json",
            success: successIzmeni,
            error: errorIzmeni
        });

        function successIzmeni(data){
            console.log(data);
            $(".info-mode").hide();
            $(".edit-mode").show();
        }
        function errorIzmeni(data){
            console.log("error");
            console.log(data);
        }
    });

    $("#dohvati_poslednje_kolo").click(function(){

        $.ajax({
            type: "POST",
            url: "/user/backoffice/dohvati_poslednje_kolo",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: successDohvati,
            error: errorDohvati
        });

        function successDohvati(data){
            prikaziKolo.html("");
            if (data.status == "ok"){
                prikaziKolo.html(data.html);
            }else{
                alert(data.message);
            }
        }
        function errorDohvati(data){
            console.log("error");
            console.log(data);
        }
    });

    $(document).on("click",".dugme-dodaj-klub", function(){
        $(".dodaj-klub").fadeIn(300);
        klubIzmena = $(this);
    });

    $(".dodaj-klub-modal .navigation>div.change-tab").click(function(){
        $('.navigation .active-tab').animate({
            left: $(this).position().left,
            marginLeft: $(this).data('show') == "lista-klubova" ? "4%" : "0"
        },200);
        $('.dodaj-klub-modal .tab').hide();
        $("."+$(this).data("show")).fadeIn(200);
    });
    $("#klub-grb").change(function(e){
        fileToUpload = e.currentTarget.files[0];
        var reader = new FileReader();
        reader.onloadend = function(e){
            if (e.target.readyState == FileReader.DONE){
                fileByteArray = e.target.result.split(',')[1];
            }
        }
        reader.readAsDataURL(fileToUpload);
    });
    $(".dodaj-klub-close").click(function(e){
        e.preventDefault();
        $(".dodaj-klub-modal").find("input[type=text], textarea, input[type=file]").val("");
        $(".dodaj-klub").fadeOut(300);
        klubIzmena = false;
    });
    $("#dodaj-klub-start").click(function(e){
        e.preventDefault();

        //var file = document.getElementById('klub-grb').files[0];
        var params = {
            ime: $("#klub-ime").val(),
            skracenica: $("#klub-skracenica").val(),
            grb : fileByteArray,
            type: fileToUpload.type,
            opis: $("#klub-opis").val()
        };

        $.ajax({
            type: "POST",
            url: "/user/backoffice/dodaj_klub",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(params),
            success: function(data){
                console.log(data);
                if (data.status == "ok"){
                    var tip = $(klubIzmena).data("tip");
                    var klubDiv = $(klubIzmena).parent().parent();
                    klubDiv.attr("data-"+tip+"-id",data.klub.id);
                    klubDiv.find('img.'+tip).attr('src', data.klub.grb);
                    klubDiv.find('.'+tip+'.ime').html(data.klub.ime);
                    $(klubIzmena).remove();
                    klubIzmena = false;
                    $(".dodaj-klub-modal").find("input[type=text], textarea, input[type=file]").val("");
                    $(".dodaj-klub").fadeOut(300);
                }else{
                    alert("greska, probaj ponovo");
                }
            },
            error: function(error){
                console.log("error");
                console.log(error);
            }
        })

    });
    $(".izaberi-klub").click(function(e){
        e.preventDefault();
        var me = $(this);
        var tip = $(klubIzmena).data("tip");
        var klubDiv = $(klubIzmena).parent().parent();
        klubDiv.attr("data-"+tip+"-id",me.data("id"));
        klubDiv.find('img.'+tip).attr('src', me.data("grb"));
        klubDiv.find('.'+tip+'.ime').html(me.data("ime"));
        $(klubIzmena).remove();
        klubIzmena = false;
        $(".dodaj-klub-modal").find("input[type=text], textarea, input[type=file]").val("");
        $(".dodaj-klub").fadeOut(300);
    })
    $(document).on("click", "#rezultat-dodaj", function(e){
        e.preventDefault();
        var rezultati = [];
        prikaziKolo.find(".rezultat").each(function(){
            var that = $(this);
            var rezultat = {
                domacin_id : Number(that.data("domacin-id")),
                gost_id : Number(that.data("gost-id")),
                golovi_domacin_ukupno : Number(that.find('.golovi_domacin_ukupno').val()),
                golovi_gost_ukupno : Number(that.find('.golovi_gost_ukupno').val()),
                golovi_domacin_poluvreme : Number(that.find('.golovi_domacin_poluvreme').val()),
                golovi_gost_poluvreme : Number(that.find('.golovi_gost_poluvreme').val())
            };

            rezultati.push(rezultat);
        });
        var data = {
            sezona : $("#novo-kolo-sezona").val(),
            kolo : Number($("#novo-kolo-kolo").val()),
            rezultati : rezultati
        };
        $.ajax({
            type: "POST",
            url: "/user/backoffice/dodaj_kolo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            dataType: "json",
            success: successDodaj,
            error: errorDodaj
        });
        function successDodaj(data){
            console.log(data);
            if (data.status == "ok"){
                sezonaSelect.val(data.sezona);
                koloSelect.val(Number(data.kolo));
                $(".info-mode").show();
                $(".edit-mode").hide();
            }
        }
        function errorDodaj(data){
            console.log("error");
            console.log(data);
        }
    });
});