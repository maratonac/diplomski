$(document).ready(function(){

    var klubovi = $("#klubovi");
    var fileByteArray = false;
    var fileToUpload = false;

    klubovi.change(function(){
        var klub = klubovi.find("option:selected");
        $("#klub-forma").attr("data-klub-id","").hide().find("input[type=text], textarea, input[type=file]").val("")
            .find("img").attr("src","");
        if(klub.val() != -1) {
            $("#klub-ime").val(klub.data("klub-ime"));
            $("#klub-skracenica").val(klub.data("klub-skracenica"));
            $("#klub-grb-slika").attr("src", klub.data("klub-grb"));
            $("klub-opis").val(klub.data("klub-opis"));
            $("#klub-forma").attr("data-klub-id",klub.val()).fadeIn(300);
        }
    });

    $(".klub-odustani").click(function(){
        console.log("odustani");
        var klub = klubovi.find("option:selected");
        $("#klub-ime").val(klub.data("klub-ime"));
        $("#klub-skracenica").val(klub.data("klub-skracenica"));
        $("#klub-grb-slika").attr("src", klub.data("klub-grb"));
        $("#klub-opis").val(klub.data("klub-opis"));
    });

    $("#klub-grb").change(function(e){
        fileToUpload = e.currentTarget.files[0];
        var reader = new FileReader();
        reader.onloadend = function(e){
            if (e.target.readyState == FileReader.DONE){
                $("#klub-grb-slika").attr("src", e.target.result);
                fileByteArray = e.target.result.split(',')[1];
            }
        }
        reader.readAsDataURL(fileToUpload);
    });

    $("#sacuvaj-klub").click(function(){

        var params = {
            klub_id : $("#klub-forma").attr("data-klub-id"),
            ime: $("#klub-ime").val(),
            skracenica: $("#klub-skracenica").val(),
            grb : fileByteArray,
            type: fileToUpload.type,
            opis: $("#klub-opis").val()
        }

        $.ajax({
            type: "POST",
            url: "/user/backoffice/izmeni_klub",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(params),
            success: function(data){
                if (data.status == "ok"){
                    alert("Uspesna izmena");
                }else{
                    alert("Nije uspela izmena");
                }
            },
            error: function(error){
                alert("Nije uspela izmena");
                console.log(error);
            }
        })
    })
});