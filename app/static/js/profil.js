$(document).ready(function(){

    $("#izmeni-start").click(function(e){
        e.preventDefault();

        var params = {
            user_id : $("#user_id").val(),
            email : $("#user-email").val(),
            username : $("#user-username").val(),
            ime : $("#user-name").val(),
            prezime : $("#user-last-name").val()
        };

        $.ajax({
            type: "POST",
            url: "/user/backoffice/izmeni_korisnika",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            success: function(data){
                if (data.status == "ok"){
                    alert("Uspesno ste izmenili podatke");
                }else{
                    alert(data.message);
                }
            },
            error: function(error){
                console.log(error);
                alert("error");
            }
        })
    });

    $("#password-start").click(function(e){
        e.preventDefault();

        var params = {
            user_id : $("#user_id").val(),
            password : $("#user-password").val(),
            cpassword : $("#user-cpassword").val()
        };

        $.ajax({
            type: "POST",
            url: "/user/izmeni_password",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(params),
            dataType: "json",
            success: function(data){
                if (data.status == "ok"){
                    $("#user-password").val("");
                    $("#user-cpassword").val("");
                    alert("Uspesno ste izmenili password");
                }else{
                    alert(data.message);
                }
            },
            error: function(error){
                console.log(error);
                alert("error");
            }
        })
    })
})