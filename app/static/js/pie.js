(function PieCtrl(window){

    PieCtrl.prototype.mainElement = false;

    PieCtrl.prototype.radius = false;
    PieCtrl.prototype.data = false;

    function PieCtrl(){}

    var color = d3.scale.ordinal();
    var svg;
    var arc;
    var pie;
    var path;

    var tip;

    PieCtrl.prototype.init = function(new_element, new_colors, new_radius, new_data){

        this.mainElement = new_element;
        color.range(new_colors);
        this.radius = new_radius;
        this.data = new_data;

        tip = d3.select(".tooltip");

        this.nacrtaj();
    };

    PieCtrl.prototype.nacrtaj = function(){

        svg = d3.select(this.mainElement).append('svg')
            .attr('width', '100%')
            .attr('height', '100%')
            .append('g')
            .attr('transform','translate(' + this.radius + ',' + this.radius + ')');

        arc = d3.svg.arc()
            .outerRadius(radius - 10)
            .innerRadius(0);

        pie = d3.layout.pie()
            .sort(null)
            .value(function(d){ return d.vrednost });

        path = svg.datum(this.data).selectAll("path")
            .data(pie)
            .enter()
            .append('path')
            .attr("fill", function(d,i){ return color(i); })
            .attr("d", arc)
            .each(function(d){this._current = d})
            .on("mouseover", function(d){
                console.log(d3.event);
                tip.transition()
                    .duration(200)
                    .style("opacity",.9);
                tip.html(
                    "<strong>" + d.data.ime + ": " + d.data.vrednost + "</strong>"
                )
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 28) + "px");
            })
            .on("mouseout", function(d){
                tip.transition()
                    .duration(200)
                    .style("opacity", 0);
            });

    };

    PieCtrl.prototype.update = function(new_data){

        this.data = new_data;

        svg.datum(this.data).selectAll("path")
            .data(pie)
            .enter()
            .append('path')
            .attr("fill", function(d,i){ return color(i); })
            .attr("d", arc)
            .each(function(d){this._current = d})
            .on("mouseover", function(d){
                tip.transition()
                    .duration(200)
                    .style("opacity",.9);
                tip.html(
                    "<strong>" + d.ime + ": " + d.vrednost + "</strong>"
                );
            })
            .on("mouseout", function(d){
                tip.transition()
                    .duration(200)
                    .style("opacity", 0);
            });

        path.transition().duration(750).attrTween("d", arcTween);

    };

    function arcTween(a) {
        var i = d3.interpolate(this._current, a);
        this._current = i(0);
        return function(t) {
            return arc(i(t));
        };
    }

    window.PieCtrl = PieCtrl;

}(window));