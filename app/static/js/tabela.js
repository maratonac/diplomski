(function(window){

    var rezultati = d3.select("#rezultati");
    var tabela = d3.select("#tabela");
    var prethodno_kolo = $("#prethodno_kolo");
    var sledece_kolo = $("#sledece_kolo");

    var data = {};
    var trenutno_kolo = -1;

    function TabelaCtrl(nova_data, novo_trenutno_kolo){

        data = nova_data;
        trenutno_kolo = novo_trenutno_kolo;

        nacrtajRezultate(trenutno_kolo);

        prethodno_kolo.click(function(){

            if (trenutno_kolo > 0){
                nacrtajRezultate(trenutno_kolo - 1);
                trenutno_kolo -= 1;
            }
        });

        sledece_kolo.click(function(){

            if (trenutno_kolo < data.kola.length - 1) {

                nacrtajRezultate(trenutno_kolo + 1);
                trenutno_kolo += 1;

            }

        });

    }

    TabelaCtrl.prototype.update = function(nova_data, novo_trenutno_kolo){
        data = nova_data;
        trenutno_kolo = novo_trenutno_kolo;
        nacrtajRezultate(trenutno_kolo);
    };

    function nacrtajRezultate(kolo){

        var noviRezultati = data.kola[kolo].rezultati;

        rezultati.selectAll('svg')
            .remove();
        rezultati
            .style('height',function(){ return (30 * noviRezultati.length) + "px";})

        var innerSvg = rezultati.selectAll('svg')
            .data(noviRezultati);

        innerSvg.enter()
            .append('svg')
            .style('opacity', 0.0)
            .attr('width', "100%")
            .attr('height', "30px")
            .attr('x', "0")
            .attr('y', function(d,i){return 30 * i + "px";})
            .transition()
            .duration(750)
            .style('opacity', 1.0);

        var innerG = innerSvg.append('g')
            .attr('class', "rezultat")
            .attr('transform', "scale(1,1)");

        innerG.append('text')
            .attr("x", "0")
            .attr("y", "15px")
            .attr("dy", ".35em")
            .text(function(d){ return d.domacin });

        innerG.append('text')
            .attr("x", "40%")
            .attr("y", "15px")
            .attr("dy", ".35em")
            .text(function(d){ return d.gost });

        innerG.append('text')
            .attr("x", "80%")
            .attr("y", "15px")
            .attr("dy", ".35em")
            .text(function(d){
                return d.golovi_domacin_ukupno + ":" + d.golovi_gost_ukupno +
                    " (" + d.golovi_domacin_poluvreme + ":" + d.golovi_gost_poluvreme + ")";
            });

        $("#kolo").html("Kolo: " + (kolo + 1));
        nacrtajTabelu(kolo);
    }

    function nacrtajTabelu(kolo){

        console.log("nacrtaj tabelu");
        var novaTabela = data.kola[kolo].tabela;

        console.log(novaTabela);

        tabela
            .style('height',function(){ return (30 * novaTabela.length) + "px";});

        tabela.selectAll('svg')
            .remove();

        var innerSvg = tabela.selectAll('svg')
            .data(novaTabela)
            .enter()
            .append('svg')
            .attr("width", "100%")
            .attr("height", "30px")
            .attr("x", 0)
            .attr("y", function(d,i){ return (30 * i) + "px";});


        console.log(innerSvg);
        innerSvg
            .append('text')
            .attr("x","0")
            .attr("y","15px")
            .attr("dy",".35em")
            .attr("class","pozicija")
            .text(function(d,i){return i+1;});
        innerSvg
            .append('text')
            .attr('x', "5%")
            .attr('y', "15px")
            .attr('dy', '.35em')
            .attr('class', 'ime_kluba')
            .text(function(d){return d.klub.ime; });
        innerSvg
            .append('text')
            .attr('x', "36%")
            .attr('y', "15px")
            .attr('dy', '.35em')
            .attr('class', 'odigrano')
            .text(function(d){return d.br_utakmica; });
        innerSvg
            .append('text')
            .attr('x', "44%")
            .attr('y', "15px")
            .attr('dy', '.35em')
            .attr('class', 'pobede')
            .text(function(d){return d.pobede; });
        innerSvg
            .append('text')
            .attr('x', "52%")
            .attr('y', "15px")
            .attr('dy', '.35em')
            .attr('class', 'nereseno')
            .text(function(d){return d.nereseno; });
        innerSvg
            .append('text')
            .attr('x', "60%")
            .attr('y', "15px")
            .attr('dy', '.35em')
            .attr('class', 'porazi')
            .text(function(d){return d.porazi; });
        innerSvg
            .append('text')
            .attr('x', "68%")
            .attr('y', "15px")
            .attr('dy', '.35em')
            .attr('class', 'golovi_dato')
            .text(function(d){return d.golovi_dato; });
        innerSvg
            .append('text')
            .attr('x', "76%")
            .attr('y', "15px")
            .attr('dy', '.35em')
            .attr('class', 'odigrano')
            .text(function(d){return d.golovi_primljeno; });
        innerSvg
            .append('text')
            .attr('x', "84%")
            .attr('y', "15px")
            .attr('dy', '.35em')
            .attr('class', 'gol_razlika')
            .text(function(d){return (parseInt(d.golovi_dato) - parseInt(d.golovi_primljeno)); });
        innerSvg
            .append('text')
            .attr('x', "92%")
            .attr('y', "15px")
            .attr('dy', '.35em')
            .attr('class', 'bodovi')
            .text(function(d){return d.bodovi; });
        //innerSvg
        //    .transition()
        //    .duration(750)


        //innerSvg
        //    .select('g')
        //    .text(function(d){
        //        return d.bodovi;
        //    });
        //
        //innerSvg
        //    .select('.ime_kluba')
        //    .text(function(d){
        //        return d.klub.ime
        //    });
    }

    window.TabelaCtrl = TabelaCtrl;

}(window));