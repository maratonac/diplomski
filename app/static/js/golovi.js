(function GoloviCtrl(window){

    GoloviCtrl.prototype.mainElement = false;
    GoloviCtrl.prototype.data = false;
    GoloviCtrl.prototype.sirina = false;
    GoloviCtrl.prototype.visina = false;

    var svg;
    var bar;
    var x;
    var y;
    var xAxis;
    var yAxis;

    var margin = {top: 20, right: 30, bottom: 30, left: 40};

    function GoloviCtrl(){}

    GoloviCtrl.prototype.init = function(mainElement, data, sirina, visina){
        this.mainElement = mainElement;
        this.data = data;
        this.sirina = sirina - margin.left - margin.right;
        this.visina = visina - margin.top - margin.bottom;

        var visinaGrafika = this.visina;

        x = d3.scale.ordinal()
            .rangeRoundPoints([0, this.sirina],.4);

        y = d3.scale.linear()
            .range([this.visina, 0]);

        xAxis = d3.svg.axis()
            .scale(x)
            .orient('bottom');
        yAxis = d3.svg.axis()
            .scale(y)
            .orient('left');

        svg = d3.select('#golovi')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100%')
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        x.domain(data.map(function(d){ return d.rezultat;}));
        y.domain([0, d3.max(data, function(d){ return d.broj; })]);

        svg.append('g')
            .attr('class', 'x axis')
            .attr('transform', 'translate(0,' + this.visina + ')')
            .call(xAxis);

        svg.append('g')
            .attr('class', 'y axis')
            .call(yAxis)
            .append('text')
            .attr('transform', 'rotate(-90)')
            .attr('y', 6)
            .attr('dy', '.71em')
            .style('text-anchor', 'end')
            .text('Broj utakmica');

        svg.selectAll('.bar')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'bar')
            .attr('x', function(d) { return x(d.rezultat) - 10;})
            .attr('width', 20)
            .attr('y', function(d) { return y(d.broj); })
            .attr('height', function(d){ return visinaGrafika - y(d.broj); });

        svg.selectAll('.bar-text')
            .data(data)
            .enter()
            .append("text")
            .attr("class", 'bar-text')
            .attr("dy", ".75em")
            .attr("y", function(d) { return y(d.broj) - 15; })
            .attr("x", function(d){ return x(d.rezultat); })
            .attr("text-anchor", "middle")
            .text(function(d){console.log(d.broj); return d.broj; });
    };

    GoloviCtrl.prototype.update = function(new_data) {
        this.data = new_data;

        x.domain(new_data.map(function(d){ return d.rezultat;}));
        y.domain([0, d3.max(new_data, function(d){ return d.broj; })]);
        var visinaGrafika = this.visina;

        svg.selectAll('.bar')
            .data(this.data)
            .enter()
            .append('rect')
            .attr('class', 'bar');

        svg.selectAll('.bar')
            .data(this.data)
            .exit()
            .remove();

        svg.selectAll('.bar-text')
            .data(this.data)
            .enter()
            .append('text')
            .attr("class", 'bar-text')
            .attr("dy", ".75em")
            .attr("text-anchor", "middle");

        svg.selectAll('.bar-text')
            .data(this.data)
            .exit()
            .remove();

        svg.selectAll('.bar').transition().duration(750)
            .attr('x', function(d) { return x(d.rezultat) - 10;})
            .attr('width', 20)
            .attr('y', function(d) { return y(d.broj); })
            .attr('height', function(d){ return visinaGrafika - y(d.broj); });

        svg.selectAll('.bar-text').transition().duration(750)
            .attr("y", function(d) { return y(d.broj) - 15; })
            .attr("x", function(d){ return x(d.rezultat); })
            .text(function(d){console.log(d.broj); return d.broj; });

        svg.select('.x.axis')
            .transition()
            .duration(750)
            .call(xAxis);
        svg.select('.y.axis')
            .transition()
            .duration(750)
            .call(yAxis);
    };

    window.GoloviCtrl = GoloviCtrl;

})(window);