from flask import request
from flask import Response, redirect
from flask.ext.login import login_required, logout_user, current_user

from . import user
import json

@user.route('/register', methods=["POST"])
def register():
    email = request.json["email"]
    username = request.json["username"]
    password = request.json["password"]
    confirmPassword = request.json["confirmPassword"]
    name = request.json["name"]
    lastName = request.json["lastName"]

    from app.model.user import User

    status, errors = User.registruj(email, username, password, confirmPassword, name, lastName)
    result = dict()
    result["status"] = status
    result["errors"] = errors
    return Response(json.dumps(result))


@user.route('/login', methods=["POST"])
def login():
    username = request.json["username"]
    password = request.json["password"]

    from app.model.user import User

    status, errors = User.login(username, password)
    result = dict()
    result["status"] = status
    result["errors"] = errors
    return Response(json.dumps(result))


@user.route('/logout', methods=["GET", "POST"])
@login_required
def logout():
    redirect_url = "/index"
    if current_user.backoffice:
        redirect_url = "/backoffice"
    logout_user()
    return redirect(redirect_url)


@user.route("/izmeni_password", methods=["POST"])
@login_required
def izmeni_password():
    from app.model.user import User

    user = User.get_by_id(request.json["user_id"])

    result = user.izmeni_password(request.json["password"], request.json["cpassword"])

    return Response(json.dumps(result))


## Backoffice
@user.route('/backoffice/login', methods=["POST"])
def backoffice_login():
    username = request.json["username"]
    password = request.json["password"]

    from app.model.user import User

    status, errors = User.login(username, password, True)
    result = dict()
    result["status"] = status
    result["errors"] = errors
    return Response(json.dumps(result))

@user.route('/backoffice/dohvati_broj_kola', methods=["POST"])
def dohvati_broj_kola():
    sezona = request.json["sezona"]

    from app.model.sezona import Sezona
    broj_kola = Sezona.dohvati_broj_kola(sezona)

    result = dict()
    result["sezona"] = sezona
    result["broj_kola"] = broj_kola

    return Response(json.dumps(result))

@user.route('/backoffice/dohvati_kolo', methods=["POST"])
def dohvati_kolo():
    sezona = request.json["sezona"]
    kolo = request.json["kolo"]

    from app.model.rezultat import Rezultat
    kolo = Rezultat.dohvati_rezultate_za_sezonu_kolo(sezona, kolo)
    print(kolo)
    from flask import render_template
    result = dict()
    if kolo:
        result["status"] = "ok"
        result["html"] = render_template("backoffice/kolo.html", kolo=kolo)
    else:
        result['status'] = "error"
        result["message"] = "Ne postoji kolo"

    return Response(json.dumps(result))

@user.route('/backoffice/izmeni_kolo', methods=["POST"])
def izmeni_kolo():

    from app.model.sezona import Sezona

    kolo = request.json["kolo"]
    rezultati = request.json["rezultati"]

    sezona = Sezona.dohvati_sezonu(request.json["sezona"])
    status = sezona.izmeni_kolo(kolo, rezultati)
    from app.jobs.formiraj_tabelu import formiraj_tabelu

    formiraj_tabelu(sezona)

    result = dict()
    result["status"] = status

    return Response(json.dumps(result))

@user.route("/backoffice/dohvati_poslednje_kolo", methods=["POST"])
def dohvati_poslednje_kolo():

    from app.jobs.dohvati_kolo import dohvati_kolo
    from app.model.sezona import Sezona
    from app.model.klub import Klub
    from app.utils.asset_url import asset_url
    rezultati = dohvati_kolo()

    sezona = Sezona.dohvati_poslednju_sezonu()
    kolo = len(sezona.kola) + 1

    poslednje_kolo = rezultati["{}".format(kolo)]

    final_kolo = list()
    for rezultat in poslednje_kolo:
        temp_result = dict()
        temp = Klub.dohvati_klub_ime(str(rezultat['domacin']))
        if temp:
            temp_result["domacin"] = temp.ime
            temp_result["domacin_id"] = temp.id
            temp_result["grb_domacin"] = asset_url("slike/{}".format(temp.grb))
            temp_result['domacin_novo'] = False
        else:
            temp_result['domacin'] = rezultat['domacin']
            temp_result["domacin_id"] = -1
            temp_result['grb_domacin'] = ''
            temp_result['domacin_novo'] = True
        temp = Klub.dohvati_klub_ime(str(rezultat['gost']))
        if temp:
            temp_result["gost"] = temp.ime
            temp_result["gost_id"] = temp.id
            temp_result["grb_gost"] = asset_url("slike/{}".format(temp.grb))
            temp_result["gost_novo"] = False
        else:
            temp_result['gost'] = rezultat['gost']
            temp_result["gost_id"] = -1
            temp_result['grb_gost'] = ''
            temp_result['gost_novo'] = True

        if ":" in rezultat["rezultat"]:
            temp_result["golovi_domacin_ukupno"] = rezultat["rezultat"].split(" ")[0].split(":")[0]
            temp_result["golovi_gost_ukupno"] = rezultat["rezultat"].split(" ")[0].split(":")[1]
        else:
            temp_result["golovi_domacin_ukupno"] = 0
            temp_result["golovi_gost_ukupno"] = 0
        if len(rezultat["rezultat"].split(" ")) == 2:
            temp_result["golovi_domacin_poluvreme"] = rezultat["rezultat"].split(" ")[1].split(":")[0].replace("(", "")
            temp_result["golovi_gost_poluvreme"] = rezultat["rezultat"].split(" ")[1].split(":")[1].replace(")", "")
        else:
            temp_result["golovi_domacin_poluvreme"] = 0
            temp_result["golovi_gost_poluvreme"] = 0
        final_kolo.append(temp_result)

    from flask import render_template
    result = dict()
    if kolo:
        result["status"] = "ok"
        result["html"] = render_template("backoffice/dodaj_kolo.html", kolo=final_kolo, sezona=sezona.sezona)
    else:
        result['status'] = "error"
        result["message"] = "Nije dohvaceno kolo"
    return Response(json.dumps(result))


@user.route("/backoffice/dodaj_klub", methods=["POST"])
def dodaj_klub():

    from app.model.klub import Klub
    result = Klub.dodaj_klub(request.json)

    return Response(json.dumps(result))


@user.route("/backoffice/izmeni_klub", methods=["POST"])
def izmeni_klub():

    from app.model.klub import Klub
    klub = Klub.dohvati_klub_id(request.json["klub_id"])
    if klub:
        result = klub.izmeni(request.json)
    else:
        result = dict()
        result["status"] = "error"
        result["message"] = "Ne postoji klub"
    return Response(json.dumps(result))


@user.route("/backoffice/dodaj_kolo", methods=["POST"])
def dodaj_kolo():
    result = dict()
    result["status"] = "ok"
    try:
        from app import db
        from app.model.sezona import Sezona
        from app.model.klub import Klub
        from app.model.rezultat import Rezultat

        rezultati = request.json["rezultati"]
        sezona = Sezona.dohvati_sezonu(request.json["sezona"])
        kolo = sezona.dodaj_kolo()
        db.session.flush()
        for rez in rezultati:
            domacin = Klub.dohvati_klub_id(int(rez["domacin_id"]))
            gost = Klub.dohvati_klub_id(int(rez["gost_id"]))
            krajnji_rezultat = "1" if rez["golovi_domacin_ukupno"] > rez["golovi_gost_ukupno"] else \
                            "2" if rez["golovi_gost_ukupno"] > rez["golovi_domacin_ukupno"] else "X"
            rezultat_golovi_konacno = "{}:{}".format(rez["golovi_domacin_ukupno"], rez["golovi_gost_ukupno"])
            rezultat_golovi_poluvreme = "{}:{}".format(rez["golovi_domacin_poluvreme"], rez["golovi_gost_poluvreme"])
            rezultat = Rezultat(domacin.id, gost.id, domacin.ime, gost.ime,
                                rez["golovi_domacin_ukupno"], rez["golovi_domacin_poluvreme"],
                                rez["golovi_gost_ukupno"], rez["golovi_gost_poluvreme"],
                                krajnji_rezultat, sezona.sezona,
                                rezultat_golovi_konacno, rezultat_golovi_poluvreme)
            kolo.dodaj_rezultat(rezultat)
        db.session.commit()

        from app.jobs.formiraj_tabelu import formiraj_tabelu
        formiraj_tabelu(sezona)

        result['sezona'] = sezona.sezona
        result['kolo'] = len(sezona.kola)
    except Exception as e:
        print(e)
        result["status"] = "error"
        result["message"] = "Greska prilikom cuvanja novog kola"

    return Response(json.dumps(result))


@user.route("/backoffice/dohvati_korisnika", methods=["POST"])
def dohvati_korisnika():

    from app.model.user import User

    korisnik = User.get_by_id(request.json["user_id"])
    if korisnik:
        result = dict()
        result["status"] = "ok"
        result["user"] = dict()
        result["user"]["id"] = korisnik.id
        result["user"]["email"] = korisnik.email
        result["user"]["username"] = korisnik.username
        result["user"]["ime"] = korisnik.ime
        result["user"]["prezime"] = korisnik.prezime
    else:
        result = dict()
        result["status"] = "error"
        result["message"] = "Nema usera sa tim id-jem"

    return Response(json.dumps(result))


@user.route("/backoffice/izmeni_korisnika", methods=["POST"])
def izmeni_korisnika():

    from app.model.user import User

    korisnik = User.get_by_id(request.json["user_id"])
    if user:
        result = korisnik.izmeni(request.json["email"], request.json["username"],
                                 request.json["ime"], request.json["prezime"])
    else:
        result = dict()
        result["status"] = "error"
        result["message"] = "Nema usera sa tim id-jem"

    return Response(json.dumps(result))
