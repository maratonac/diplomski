from . import data

import os
from os import getcwd
from flask import Response, make_response
from flask import request


@data.route('/dohvati_sezonu', methods=["POST"])
def dohvati_sezonu():

    from app.model.sezona import Sezona

    sezona = Sezona.dohvati_sezonu(request.json["sezona"])
    if sezona:
        from app.utils.SezoneJSONEncoder import SezonaJSONEncoder
        import json
        result = dict()
        result["status"] = "ok"
        result["sezona"] = json.dumps(sezona, cls=SezonaJSONEncoder)
        return Response(json.dumps(result))

    result = dict()
    result["status"] = "error"
    result["message"] = "Ne postoji sezona"
    return Response(json.dumps(result))


@data.route('/rezultati', methods=["POST", "GET"])
def temp():

    klub_id = request.json["klub_id"]
    sezone = request.json["sezone"]
    protivnici = False
    if "protivnici" in request.json:
        protivnici = request.json["protivnici"]
    if not klub_id and not sezone:
        return Response("ERR_BAD_REQUEST")

    from app.model.rezultat import Rezultat
    rezultati = Rezultat.dohvati_odnos_pobede_porazi(klub_id, sezone, protivnici)
    print(rezultati)
    import json
    return Response(json.dumps(rezultati))

@data.route('/forma', methods=["POST"])
def forma():

    klub_id = request.json["klub_id"]
    sezone = request.json["sezone"]
    protivnici = False
    if "protivnici" in request.json:
        protivnici = request.json["protivnici"]
    if not klub_id and not sezone:
        return Response("ERR_BAD_REQUEST")

    limit = 10
    from app.model.rezultat import Rezultat

    rezultati = Rezultat.dohvati_formu(klub_id, sezone, limit, protivnici)

    import json
    return Response(json.dumps(rezultati))

@data.route('/golovi', methods=["POST"])
def rezultati():

    klub_id = request.json["klub_id"]
    sezone = request.json["sezone"]
    protivnici = False

    if "protivnici" in request.json:
        protivnici = request.json["protivnici"]

    if not klub_id and not sezone:
        return Response("ERR_BAD_REQUEST")

    from app.model.rezultat import Rezultat

    rezultati = Rezultat.dohvati_rezultate(klub_id, sezone, protivnici)

    import json
    return Response(json.dumps(rezultati))
