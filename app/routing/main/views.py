from flask import render_template
from app import db
from flask.ext.login import login_required, current_user
from . import main


@main.route('/')
@main.route('/index')
def index():
    #
    # # return render_template('index.html', sezona=trenutna_sezona, ime_lige=liga.ime,
    # #                         data=json.dumps(trenutna_sezona, cls=SezonaJSONEncoder))
    # return render_template('index.html')
    from flask import redirect
    return redirect('/tabela')


@main.route('/tabela')
def tabela():
    from app.model.liga import Liga
    from app.model.sezona import Sezona

    trenutna_sezona = Sezona.dohvati_poslednju_sezonu()

    sezone = Sezona.dohvati_sve_sezone()

    user = current_user.to_json() if current_user.get_id() is not None else False

    return render_template('tabela.html', sezone=sezone,
                           trenutna_sezona=trenutna_sezona.sezona, current_user=user)


@main.route('/klubovi')
def klubovi():

    from app.model.klub import Klub

    klubovi = Klub.dohvati_sve_klubove()

    user = current_user.to_json() if current_user.get_id() is not None else False

    return render_template('klubovi.html', klubovi=klubovi, current_user=user)


@main.route('/klubovi/<klub>')
@login_required
def klub(klub):

    from app.model.klub import Klub
    from app.model.sezona import Sezona

    klub = Klub.dohvati_klub_skracenica(klub)
    klubovi = Klub.dohvati_sve_klubove_dict()
    print(klubovi)
    sezone = Sezona.dohvati_sve_sezone()
    if not klub:
        print("nema klub")

    user = current_user.to_json() if current_user.get_id() is not None else False

    return render_template('klub.html', klub=klub, klubovi=klubovi, sezone=sezone, current_user=user)


@main.route('/profil')
@login_required
def profil():

    user = current_user.to_json() if current_user.get_id() is not None else False

    return render_template('profil.html', current_user=user, user=current_user)