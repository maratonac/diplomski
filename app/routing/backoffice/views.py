from . import backoffice

import os
from os import getcwd
from flask import render_template, Response, make_response
from flask import request, redirect
from flask.ext.login import login_required, current_user, logout_user


@backoffice.route("/")
def backoffice_login():
    user = current_user.to_json() if current_user.get_id() is not None else False
    if user:
        logout_user()
    return render_template("backoffice/login.html", current_user=user)


@backoffice.route("/rezultati")
@login_required
def results_page():
    user = current_user.to_json() if current_user.get_id() is not None else False
    from app.model.sezona import Sezona
    from app.model.klub import Klub
    sezone = Sezona.dohvati_sve_sezone()
    klubovi = Klub.dohvati_sve_klubove()

    return render_template("backoffice/rezultati.html", current_user=user, sezone=sezone, klubovi=klubovi)


@backoffice.route("/klubovi")
@login_required
def klubovi_page():
    user = current_user.to_json() if current_user.get_id() is not None else False
    from app.model.klub import Klub
    klubovi = Klub.dohvati_sve_klubove()

    return render_template("backoffice/klubovi.html", current_user=user, klubovi=klubovi)

@backoffice.route("/korisnici")
@login_required
def users_page():
    user = current_user.to_json() if current_user.get_id() is not None else False
    from app.model.user import User
    korisnici = User.get_all()
    return render_template("backoffice/korisnici.html", current_user=user, korisnici=korisnici)

