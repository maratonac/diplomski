def asset_url(url):
    from flask import current_app

    return current_app.config.get('ASSET_PATH', '/static') + url
