from app import db
import json


class JSONEncodedDict(db.TypeDecorator):

    impl = db.TEXT

    def process_bind_param(self, value, dialect):
        if value:
            value = json.dumps(value)

        return value

    def process_result_value(self, value, dialect):
        if value:
            value = json.loads(value)
        return value


class StringChoiceType(db.TypeDecorator):
    impl = db.String

    def __init__(self, choices, **kw):
        self.choices = dict(choices)
        super(self.__class__, self).__init__(**kw)

    def process_bind_param(self, value, dialect):
        return [k for k, v in self.choices.items() if v == value].pop()

    def process_result_value(self, value, dialect):
        return self.choices[value]
