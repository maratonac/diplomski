from json import JSONEncoder
from app.utils.asset_url import asset_url


class KlubJSONEncoder(JSONEncoder):

    def default(self, o):
        temp_klub = dict()
        temp_klub["ime"] = o.ime
        temp_klub["skracenica"] = o.skracenica
        temp_klub["grb"] = asset_url(o.grb)
        return temp_klub
