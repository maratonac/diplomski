from json import JSONEncoder


class SezonaJSONEncoder(JSONEncoder):

    def default(self, o):
        temp_sezona = dict()
        temp_sezona["sezona"] = o.sezona
        temp_sezona["broj_kola"] = o.broj_kola
        temp_sezona["trenutno_kolo"] = o.trenutno_kolo
        temp_sezona["kola"] = list()
        for kolo in o.kola:
            temp_kolo = dict()
            temp_kolo["broj_kola"] = kolo.broj_kola
            temp_kolo["tabela"] = kolo.tabela
            temp_kolo["rezultati"] = list()
            for rez in kolo.rezultati:
                temp_rezultat = dict()
                temp_rezultat["domacin"] = rez.domacin
                temp_rezultat["gost"] = rez.gost
                temp_rezultat["golovi_domacin_ukupno"] = rez.golovi_domacin_ukupno
                temp_rezultat["golovi_domacin_poluvreme"] = rez.golovi_domacin_poluvreme
                temp_rezultat["golovi_gost_ukupno"] = rez.golovi_gost_ukupno
                temp_rezultat["golovi_gost_poluvreme"] = rez.golovi_gost_poluvreme
                temp_rezultat["rezultat"] = rez.rezultat
                temp_kolo["rezultati"].append(temp_rezultat)
            temp_sezona["kola"].append(temp_kolo)

        return temp_sezona
