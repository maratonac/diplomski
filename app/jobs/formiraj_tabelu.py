from app import db


def formiraj_tabelu(sezona=False):
    try:
        if sezona:
            for kolo in sezona.kola:
                tabela = list()
                prethodno_kolo = False
                if kolo.broj_kola > 0:
                    prethodno_kolo = sezona.kola[kolo.broj_kola - 1]
                for rezultat in kolo.rezultati:
                    from app.model.klub import Klub
                    domacin = dict()
                    gost = dict()
                    temp = Klub.dohvati_klub_id(rezultat.id_domacin)
                    domacin["klub"] = dict()
                    domacin["klub"]["id"] = temp.id
                    domacin["klub"]["ime"] = temp.ime
                    domacin["klub"]["grb"] = temp.grb
                    temp = Klub.dohvati_klub_id(rezultat.id_gost)
                    gost["klub"] = dict()
                    gost["klub"]["id"] = temp.id
                    gost["klub"]["ime"] = temp.ime
                    gost["klub"]["grb"] = temp.grb
                    domacin["br_utakmica"] = kolo.broj_kola + 1
                    gost["br_utakmica"] = kolo.broj_kola + 1
                    if rezultat.rezultat == "1":
                        domacin["pobede"] = 1
                        domacin["nereseno"] = 0
                        domacin["porazi"] = 0
                        domacin["bodovi"] = 3
                        gost["pobede"] = 0
                        gost["nereseno"] = 0
                        gost["porazi"] = 1
                        gost["bodovi"] = 0
                    elif rezultat.rezultat == "X":
                        domacin["pobede"] = 0
                        domacin["nereseno"] = 1
                        domacin["porazi"] = 0
                        domacin["bodovi"] = 1
                        gost["pobede"] = 0
                        gost["nereseno"] = 1
                        gost["porazi"] = 0
                        gost["bodovi"] = 1
                    else:
                        domacin["pobede"] = 0
                        domacin["nereseno"] = 0
                        domacin["porazi"] = 1
                        domacin["bodovi"] = 0
                        gost["pobede"] = 1
                        gost["nereseno"] = 0
                        gost["porazi"] = 0
                        gost["bodovi"] = 3

                    domacin["golovi_dato"] = rezultat.golovi_domacin_ukupno
                    domacin["golovi_primljeno"] = rezultat.golovi_gost_ukupno
                    gost["golovi_dato"] = rezultat.golovi_gost_ukupno
                    gost["golovi_primljeno"] = rezultat.golovi_domacin_ukupno

                    if prethodno_kolo:
                        for elem in prethodno_kolo.tabela:
                            if elem["klub"]["id"] == domacin["klub"]["id"]:
                                domacin["pobede"] += elem["pobede"]
                                domacin["nereseno"] += elem["nereseno"]
                                domacin["porazi"] += elem["porazi"]
                                domacin["bodovi"] += elem["bodovi"]
                                domacin["golovi_dato"] += elem["golovi_dato"]
                                domacin["golovi_primljeno"] += elem["golovi_primljeno"]
                            if elem["klub"]["id"] == gost["klub"]["id"]:
                                gost["pobede"] += elem["pobede"]
                                gost["nereseno"] += elem["nereseno"]
                                gost["porazi"] += elem["porazi"]
                                gost["bodovi"] += elem["bodovi"]
                                gost["golovi_dato"] += elem["golovi_dato"]
                                gost["golovi_primljeno"] += elem["golovi_primljeno"]
                    tabela.append(domacin)
                    tabela.append(gost)
                sortirana_tabela = sorted(tabela, key=lambda temp: temp["golovi_dato"], reverse=True)
                sortirana_tabela = sorted(sortirana_tabela, key=lambda temp: temp["golovi_dato"] - temp["golovi_primljeno"],
                                          reverse=True)
                sortirana_tabela = sorted(sortirana_tabela, key=lambda temp: temp["bodovi"], reverse=True)
                print(sortirana_tabela)
                kolo.tabela = sortirana_tabela
            db.session.commit()
        else:
            print("ne postoji sezona")
            return "error"
        return "ok"
    except Exception as e:
        print("greska")
        print(e)
        return "error"

