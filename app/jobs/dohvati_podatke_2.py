from collections import deque
from urllib.parse import urldefrag, urljoin, urlparse
import bs4
import requests
import re

start_page = "http://www.srbijasport.net/rez/"

def dohvati_igru(tag):
    return tag and tag.has_attr('class') and "game" in tag["class"] and "well" in tag["class"]

def dohvati_rezultat(tag):
    return tag and tag.has_attr('class') and "result" in tag['class']

def dohvati_tim(tag):
    return tag and tag.has_attr('class') and 'team' in tag["class"]

def dohvati_poluvreme(tag):
    return tag and tag.has_attr('class') and 'game-info' in tag["class"]

def dohvati_podatke_2():

    sezone = dict()
    # sezone["2006/07"] = dict()
    # sezone["2006/07"]["regular"] = list()
    # sezone["2006/07"]["playoff"] = list()
    # sezone["2006/07"]["playout"] = list()
    # sezone["2007/08"] = dict()
    # sezone["2007/08"]["regular"] = list()
    sezone["2015/16"] = dict()
    sezone["2015/16"]["regular"] = list()
    sezone["2015/16"]["playoff"] = list()
    sezone["2015/16"]["playout"] = list()
    # for i in range(4967, 5099):
    #     sezone["2006/07"]["regular"].append(i)
    # for i in range(11444, 11474):
    #     sezone["2006/07"]["playoff"].append(i)
    # for i in range(11474, 11504):
    #     sezone["2006/07"]["playout"].append(i)
    #
    # for i in range(13796, 13928):
    #     sezone["2007/08"]["regular"].append(i)
    # for i in range(21036, 21102):
    #     sezone["2007/08"]["regular"].append(i)

    for i in range(268233, 268473):
        sezone["2015/16"]["regular"].append(i)
    for i in range(324317, 324345):
        sezone["2015/16"]["playout"].append(i)
    for i in range(324289, 324317):
        sezone["2015/16"]["playoff"].append(i)
    url = start_page

    konacni_rezultati = dict()

    for key,value in sezone.items():
        konacni_rezultati[key] = dict()
        kolo = 0
        for i in range(0, len(value["regular"])):
            if i % 8 == 0:
                kolo += 1
            if kolo not in konacni_rezultati[key]:
                konacni_rezultati[key][kolo] = list()
            url = "{}{}".format(start_page, value["regular"][i])
            print("Regular: ", url)
            try:
                response = requests.get(url)
            except Exception as e:
                print("Failed: ", url)
                return

            soup = bs4.BeautifulSoup(response.text, "html.parser")
            igra = soup.find(dohvati_igru)
            rezultat = igra.find(dohvati_rezultat)
            ekipe = igra.find_all(dohvati_tim)
            poluvreme = igra.find(dohvati_poluvreme)
            konacan_rezultat = dict()
            konacan_rezultat["domacin"] = ekipe[0].string.strip()
            konacan_rezultat["gost"] = ekipe[1].string.strip()
            konacan_rezultat["rezultat"] = "{} {}".format(rezultat.string.strip(), poluvreme.string.strip())
            konacni_rezultati[key][kolo].append(konacan_rezultat)
        if "playoff" in value:
            for i in range(0, len(value["playoff"])):
                if i % 4 == 0:
                    kolo += 1
                if kolo not in konacni_rezultati[key]:
                    konacni_rezultati[key][kolo] = list()
                url = "{}{}".format(start_page, value["playoff"][i])
                print("Playoff: ", url)
                try:
                    response = requests.get(url)
                except Exception as e:
                    print("Failed: ", url)
                    return

                soup = bs4.BeautifulSoup(response.text, "html.parser")
                igra = soup.find(dohvati_igru)
                rezultat = igra.find(dohvati_rezultat)
                ekipe = igra.find_all(dohvati_tim)
                poluvreme = igra.find(dohvati_poluvreme)
                konacan_rezultat = dict()
                konacan_rezultat["domacin"] = ekipe[0].string.strip()
                konacan_rezultat["gost"] = ekipe[1].string.strip()
                konacan_rezultat["rezultat"] = "{} {}".format(rezultat.string.strip(), poluvreme.string.strip())
                konacni_rezultati[key][kolo].append(konacan_rezultat)
                url = "{}{}".format(start_page, value["playout"][i])
                print("Playout: ", url)
                try:
                    response = requests.get(url)
                except Exception as e:
                    print("Failed: ", url)
                    return

                soup = bs4.BeautifulSoup(response.text, "html.parser")
                igra = soup.find(dohvati_igru)
                rezultat = igra.find(dohvati_rezultat)
                ekipe = igra.find_all(dohvati_tim)
                poluvreme = igra.find(dohvati_poluvreme)
                konacan_rezultat = dict()
                konacan_rezultat["domacin"] = ekipe[0].string.strip()
                konacan_rezultat["gost"] = ekipe[1].string.strip()
                konacan_rezultat["rezultat"] = "{} {}".format(rezultat.string.strip(), poluvreme.string.strip())
                konacni_rezultati[key][kolo].append(konacan_rezultat)

    upisi_u_fajl(konacni_rezultati)

def upisi_u_fajl(konacni_rezultati):
    fajl = open("stari_rezultati2.txt", "w", encoding="utf-8")
    for key, value in konacni_rezultati.items():
        broj_kola = int(max(value.keys(), key=int))
        fajl.write("{},{}\n".format(key, broj_kola))
        print(key)
        for kolo in range(1, broj_kola+1):
            trenutno_kolo = False
            if kolo in value:
                trenutno_kolo = value[kolo]
            if trenutno_kolo:
                fajl.write("{}\n".format(kolo))
                for rez in trenutno_kolo:
                    print(rez)
                    fajl.write("{}\n".format(rez))
    fajl.close()

if __name__ == "__main__":
    from app import create_app
    app = create_app()
    app_context = app.app_context()
    app_context.push()
    dohvati_podatke_2()
    app_context.pop()
