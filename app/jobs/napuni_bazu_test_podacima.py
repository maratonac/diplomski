from app import db


def napuni_bazu_test_podacima():
    try:
        from app.model.liga import Liga

        liga = Liga('Superliga', 'sl')
        db.session.add(liga)
        db.session.flush()

        from app.model.sezona import Sezona
        from app.model.klub import Klub
        from app.model.rezultat import Rezultat
        import json

        fajl = open("rezultati.txt", "r", encoding="utf-8")

        linija = fajl.readline()
        while linija != "":
            sezonaBrojKolaUtakmice = linija.split(',')
            trenutna_sezona = sezonaBrojKolaUtakmice[0]
            broj_kola = int(sezonaBrojKolaUtakmice[1].strip())
            broj_utakmica = int(sezonaBrojKolaUtakmice[2].strip())

            sezona = Sezona(trenutna_sezona, broj_kola)
            liga.sezone.append(sezona)
            db.session.flush()
            for kolo in range(1, broj_kola + 1):
                linija = fajl.readline()
                print("Sezona: {}, kolo: {}".format(trenutna_sezona, linija))
                kolo = sezona.dodaj_kolo()
                for rez in range(1, broj_utakmica + 1):
                    linija = fajl.readline()
                    print(linija)
                    rezultat = json.loads(linija, encoding="utf-8")
                    domacin = Klub.dohvati_klub_ime(rezultat["domacin"])
                    if not domacin:
                        domacin = Klub(rezultat["domacin"], "", "")
                        db.session.add(domacin)
                        liga.klubovi.append(domacin)
                        db.session.flush()
                    gost = Klub.dohvati_klub_ime(rezultat["gost"])
                    if not gost:
                        gost = Klub(rezultat["gost"], "", "")
                        db.session.add(gost)
                        liga.klubovi.append(gost)
                        db.session.flush()
                    tempRez = rezultat["rezultat"].split(' ')
                    golovi_domacin_ukupno = int(tempRez[0].split(":")[0])
                    golovi_gost_ukupno = int(tempRez[0].split(":")[1])
                    tempRezPoluvreme = tempRez[1].replace("(", "").replace(")", "")
                    golovi_domacin_poluvreme = int(tempRezPoluvreme.split(":")[0])
                    golovi_gost_poluvreme = int(tempRezPoluvreme.split(":")[1])
                    krajnji_rezultat = "1" if golovi_domacin_ukupno > golovi_gost_ukupno else \
                        "2" if golovi_gost_ukupno > golovi_domacin_ukupno else "X"
                    rezultat_golovi_konacno = "{}:{}".format(golovi_domacin_ukupno, golovi_gost_ukupno)
                    rezultat_golovi_poluvreme = "{}:{}".format(golovi_domacin_poluvreme, golovi_gost_poluvreme)
                    rezultat = Rezultat(domacin.id, gost.id, domacin.ime, gost.ime,
                                        golovi_domacin_ukupno, golovi_domacin_poluvreme,
                                        golovi_gost_ukupno, golovi_gost_poluvreme,
                                        krajnji_rezultat, sezona.sezona,
                                        rezultat_golovi_konacno, rezultat_golovi_poluvreme)

                    kolo.dodaj_rezultat(rezultat)

            linija = fajl.readline()

        db.session.commit()
        print("all done!")
    except Exception as e:
        print("greska")
        print(e)
        db.session.rollback()


if __name__ == "__main__":
    from app import create_app
    app = create_app()
    app_context = app.app_context()
    app_context.push()
    napuni_bazu_test_podacima()
    app_context.pop()
