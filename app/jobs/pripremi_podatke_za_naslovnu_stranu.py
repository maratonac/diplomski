from app import create_app, db


def pripremi_podatke_za_naslovnu_stranu():
    from app.model.liga import Liga
    from app.model.klub import Klub
    liga = Liga.nadji_ligu_po_skracenici("sl")
    if liga:
        # PRVI GRAFIK -> Broj osvojenih titula
        rezultat = dict()
        for sezona in liga.sezone:
            klub_id = sezona.sampion
            klub = Klub.dohvati_klub_id(klub_id)
            if klub.ime not in rezultat:
                rezultat[klub.ime] = 0
            rezultat[klub.ime] += 1

        print(rezultat)
        #TODO upisi u fajl

        # DRUGI GRAFIK -> rezultati
        from app.model.rezultat import Rezultat
        rezultat = Rezultat.dohvati_podatke_za_drugi_grafik()
        print(rezultat)
        #TODO upisi u fajl

        # TRECI GRAFIK ->
        rezultat = Rezultat.dohvati_podatke_za_treci_grafik()
        print(rezultat)
        #TODO upisi u fajl


if __name__ == "__main__":
    app = create_app()
    app_context = app.app_context()
    app_context.push()
    pripremi_podatke_za_naslovnu_stranu()
    app_context.pop()