from collections import deque
from urllib.parse import urljoin, urlparse
import bs4
import requests
import re

page_to_fetch = "http://superliga.rs/super-liga/rezultati-jesen"

def dohvati_tabele(tag):
    return tag and tag.name == "table" and tag.has_attr('class') and "jsltabela" in tag['class']

def nadji_kolo(tabela):
    rezultat = tabela.find_parent('article').contents[0]

    if rezultat.name == "h3":
        return rezultat.string
    elif rezultat.name == "span":
        return rezultat.find('strong').text
    elif rezultat.name == "strong":
        return rezultat.text
    return ""

def dohvati_kolo(page=page_to_fetch):

    url = page

    konacni_rezultati = dict()

    try:
        s = requests.Session()
        s.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36'
        response = s.get(url)
    except (requests.exceptions.MissingSchema,
            requests.exceptions.InvalidSchema):
        print("Failed: ", url)

    soup = bs4.BeautifulSoup(response.text, "html.parser")
    tabele = soup.find_all(dohvati_tabele)
    for tabela in tabele:
        if len(tabela.find_all('tr')[0].find_all('td')) == 5:
            kolo = nadji_kolo(tabela)
            broj_kola = kolo.split(",")[1].split("kolo")[0].replace(".", "").strip()
            konacni_rezultati[broj_kola] = list()
            rezultati = tabela.find_all('tr')[1:]
            for rezultat in rezultati:
                konacan_rezultat = dict()
                polja = rezultat.find_all('td')
                konacan_rezultat["domacin"] = polja[1].find('strong').text if polja[1].find('strong') \
                    else polja[1].string
                konacan_rezultat["gost"] = polja[2].find('strong').text if polja[2].find('strong') \
                    else polja[2].string
                konacan_rezultat["rezultat"] = polja[3].string

                konacni_rezultati[broj_kola].append(konacan_rezultat)

    return konacni_rezultati


if __name__ == "__main__":
    from app import create_app
    app = create_app()
    app_context = app.app_context()
    app_context.push()
    app_context.pop()
