from collections import deque
from urllib.parse import urldefrag, urljoin, urlparse
import bs4
import requests
import re

start_page = "http://superliga.rs/index.php"


def dohvati_arhivu(href):
    return href and re.compile("arhiva").search(href) and re.compile("rezultati").search(href)


def dohvati_tabele(tag):
    return tag and tag.name == "table" and tag.has_attr('class') and "jsltabela" in tag['class']


def nadji_kolo(tabela):
    rezultat = tabela.find_parent('article').contents[0]

    if rezultat.name == "h3":
        return rezultat.string
    elif rezultat.name == "span":
        return rezultat.find('strong').text
    elif rezultat.name == "strong":
        return rezultat.text
    return ""


def dohvati_strelce(tag):
    return tag and tag.name == "h3" and tag.string and \
           ("Strelci" in tag.string or "Golov" in tag.string)

def dohvati_podatke():

    pageque = deque()
    pageque.append(start_page)
    url = start_page

    konacni_rezultati = dict()

    try:
        response = requests.get(url)
    except (requests.exceptions.MissingSchema,
            requests.exceptions.InvalidSchema):
        print("Failed: ", url)

    soup = bs4.BeautifulSoup(response.text, "html.parser")
    links = [tag['href'] for tag in soup.find_all(href=dohvati_arhivu)]
    links = [link if bool(urlparse(link).netloc) else urljoin(start_page,link) for link in links]
    for link in links:
        # dohvati pojedinacnu stranu
        print("Fetch data from: ", link)
        temp = link.split('/')[-2].split('-')
        trenutna_sezona = "{}/{}".format(temp[1], temp[2])
        if trenutna_sezona not in konacni_rezultati:
            konacni_rezultati[trenutna_sezona] = dict()
        try:
            response = requests.get(link)
        except (requests.exception.MissingSchema,
                requests.exceptions.InvalidSchema):
            print("Failed: ", link)
        soup = bs4.BeautifulSoup(response.text, "html.parser")
        tabele = soup.find_all(dohvati_tabele)
        for tabela in tabele:
            if len(tabela.find_all('tr')[0].find_all('td')) == 5:
                kolo = nadji_kolo(tabela)
                broj_kola = kolo.split(",")[1].split("kolo")[0].replace(".", "").strip()
                konacni_rezultati[trenutna_sezona][broj_kola] = list()
                print("Sezona: {}, kolo: {}".format(trenutna_sezona, broj_kola))
                rezultati = tabela.find_all('tr')[1:]
                for rezultat in rezultati:
                    konacan_rezultat = dict()
                    polja = rezultat.find_all('td')
                    konacan_rezultat["domacin"] = polja[1].find('strong').text if polja[1].find('strong') \
                        else polja[1].string
                    konacan_rezultat["gost"] = polja[2].find('strong').text if polja[2].find('strong') \
                        else polja[2].string
                    konacan_rezultat["rezultat"] = polja[3].string

                    konacni_rezultati[trenutna_sezona][broj_kola].append(konacan_rezultat)
    upisi_u_fajl(konacni_rezultati)


def upisi_u_fajl(konacni_rezultati):
    fajl = open("stari_rezultati2.txt", "w", encoding="utf-8")
    for key, value in konacni_rezultati.items():
        broj_kola = int(max(value.keys(), key=int))
        fajl.write("{},{}\n".format(key, broj_kola))
        print(key)
        for kolo in range(1, broj_kola+1):
            trenutno_kolo = False
            if str(kolo) in value:
                trenutno_kolo = value[str(kolo)]
            if trenutno_kolo:
                fajl.write("{}\n".format(kolo))
                for rez in trenutno_kolo:
                    print(rez)
                    fajl.write("{}\n".format(rez))
    fajl.close()


if __name__ == "__main__":
    from app import create_app
    app = create_app()
    app_context = app.app_context()
    app_context.push()
    dohvati_podatke()
    app_context.pop()
