import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    LOG_FILE_PATH = basedir + '/logs/'

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'moja veoma zajebana sifra'
    SESSION_TYPE = 'filesystem' # ?
    WEB_ADDRESS = '127.0.0.1:5000'

    PROTOCOL = 'http'

    ASSET_PATH = '/static/'

    ABSOLUTE_PATH = "D:\workspace\diplomski\app"

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @staticmethod
    def init_app(app):
        pass


class DevelopementConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://diplomski:diplomski@localhost:3306/diplomski?charset=utf8&use_unicode=1"
    WEB_ADDRESS = '127.0.0.1:5000'

config = {
    'developement': DevelopementConfig,
    'default': DevelopementConfig
}
