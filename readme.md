### Diplomski

#### Potrebno:
- Python 3.5.1
- MySql Server

#### Pokretanje projekta pomocu pyCharm-a
- Kreirati bazu diplomski:
- Kreirati usera: diplomski(password: diplomski)

-napraviti dve konfiguracije: run i update:
-- update se pokrece da bi update-ovala sve promene na bazi (koristi se SQLALchemy)
-- gadja se fajl run.py u root-u, sa parametrima: db upgrade

-- run pokrece projekat
-- gadja se fajl run.py u root-u, bez parametara

-pre nego sto se pokrene projekat, potrebno je napuniti bazu test podacima tako sto
se pokrene fajl napuni_bazu_test_podacima.py koji se nalazi u app/jobs, a zatim se
pokrene fajl formiraj_tabele.py

#### Backoffice
- url je /backoffice
- backoffice korisnika je potrebno rucno uneti u bazu
<RECORDS>
	<RECORD>
		<id>1</id>
		<email>admin@admin.com</email>
		<username>admin</username>
		<password>admin</password>
		<ime>admin</ime>
		<prezime>admin</prezime>
		<platio></platio>
		<datum_isteka></datum_isteka>
		<backoffice>1</backoffice>
	</RECORD>
</RECORDS>

